import { Component, OnInit } from '@angular/core';
import { Feeds,Feed } from './feeds-data';
import { UtilityProvider } from '../../../_helpers';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.css']
})
export class FeedsComponent implements OnInit {

  feeds:Feed[];

  constructor(
    public utility:UtilityProvider
  ) {

    this.feeds = Feeds;
  }

  ngOnInit(): void {
  }

}
