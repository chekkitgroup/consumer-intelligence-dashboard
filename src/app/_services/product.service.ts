import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User, Product } from '../_models';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { Observable, of, BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ProductService {
    token: any;
    baseUrl = environment.apiUrl + environment.basePath;

    public productSource = new BehaviorSubject<Product>(null);
    productDetails$ = this.productSource.asObservable();

    public batchSource = new BehaviorSubject<any>(null);
    batchDetails$ = this.batchSource.asObservable();


    constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    }


    //=======================UPDATE PRODUCT DETAILS========================================

    updateProductDetails(productDetails: Product){
        this.productSource.next(productDetails);
    }

    //=======================UPDATE BATCH DETAILS========================================

    updateBatchDetails(batchDetails){
        this.batchSource.next(batchDetails);
    }

    getProduct(slug: string) {
        return this.http.get(this.baseUrl + `/products/${slug}`);
    }
    
    getAllUserProducts(userId: number) {
        return this.http.get(this.baseUrl + `/products/all_products/${userId}`);
    }
    getAllUserProductReports(userId: number) {
        return this.http.get(this.baseUrl + `/products/product-reports/${userId}`);
    }
    getUserLineGRaphAnalytics(userId: number) {
        return this.http.get(this.baseUrl + `/products/line-graph/${userId}`);
    }
    getUserproductSubProductAnalytics(userId: number) {
        return this.http.get(this.baseUrl + `/products/pb-line-graph/${userId}`);
    }
    getUserRewardPieChartAnalytics(userId: number) {
        return this.http.get(this.baseUrl + `/products/rewards-piegraph/${userId}`);
    }
    getUserScanHistryAnalytics(userId: number) {
        return this.http.get(this.baseUrl + `/products/scanhist-graph/${userId}`);
    }
    getUserSurveyQestRespAnalytics(userId: number) {
        return this.http.get(this.baseUrl + `/products/surveyqustresp-graph/${userId}`);
    }
    getAllProductBatches(productId: number) {
        return this.http.get(this.baseUrl + `/products/all_product_batches/${productId}`);
    }
    getAllUserProductBatches(userId: number) {
        return this.http.get(this.baseUrl + `/products/all_user_product_batches/${userId}`);
    }
    getLastExportHistory( batch_num) {
        return this.http.get(this.baseUrl + `/products/get-user-pin-export-history/${batch_num}`);
    }
    updatePinExportHistory(userId: number, data: any) {
        return this.http.post(this.baseUrl + `/products/update-pin-export-history/${userId}`, data);
    }
    create_product(product: any) {

        let payloadObj = {};
        
        // for(var pair of product.entries()){
        //     console.log(pair)
        // payloadObj[pair[0]] = pair[1];
        // }
        // for (var i = 0; i < product.length; i += 1) {
        //     var x=product[i];
        //     // formData.append(x.NAME, x.VALUE);
        //     console.log(x);
        // }
        // // for (const key in product) {
        //     console.log(product[key])

        //     fd.append(key, formDataDictionary[key]);
        //   }
        // product.forEach(function(value, key){
        //     payloadObj[key] = value;
        // });
        // console.log(payloadObj);
        // console.log(product);


        return this.http.post(this.baseUrl + `/products/`,
         
        product);
        
    }
    create_batch(sub_product: any) {
        return this.http.post(this.baseUrl + `/products/sub-products/`, sub_product);
    }
    createReward(id: number, survey_reward: any) {
        return this.http.post(this.baseUrl + `/survey-reward/${id}`, survey_reward);
    }
    generate_pin(batchProductId, product_batch: any) {
        return this.http.post(this.baseUrl + `/products/generate-pin/${batchProductId}`, product_batch);
    }
    getAllProductReward(user_id: number) {
        return this.http.get(this.baseUrl + `/survey-reward/${user_id}`);
    }
    attach_reward(product: any, productId) {
        return this.http.post(this.baseUrl + `/survey-reward/attach-reward/${productId}/${product.rewardId}`, product);
    }
    getAllUserProductPins(userId: number) {
        return this.http.get(this.baseUrl + `/products/get-userpin-generated/${userId}`);
    }
    getProductPins(slug: any, per_page: number, pin_prev = "", pin_next = "") {
        return this.http.get(this.baseUrl + `/products/get-product-pins/${slug}?per_page=${per_page}&pin_prev=${pin_prev}&pin_next=${pin_next}`);
    }
    getProductPinsExport(slug: any, per_page: number, pin_prev = "", pin_next = "") {
        return this.http.get(this.baseUrl + `/products/get-product-pins-for-export/${slug}?per_page=${per_page}&pin_prev=${pin_prev}&pin_next=${pin_next}`);
    }
    getAllProductPins(slug: any) {
        return this.http.get(this.baseUrl + `/products/get-all-product-pins/${slug}`);
    }
    delete_product(id: number) {
        return this.http.delete(this.baseUrl + `/products/${id}`);
    }
    update_product(slug: any, productData: any) {
        return this.http.put(this.baseUrl + `/products/update-product/${slug}`, productData);
    }
    update_subProduct(batch_num: any, productData: any) {
        return this.http.put(this.baseUrl + `/products/update-sub-product/${batch_num}`, productData);
    }

// analytics calls
    getScansByDate(id: any,startDate,endDate) {
        return this.http.get(this.baseUrl + `/products/get-scan-statistics-by-date/${id}/${startDate}/${endDate}`);
    }

    getProductScans(id: any) {
        return this.http.get(this.baseUrl + `/products/get-product-scan-statistics/${id}`);
    }
}