import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';


// import 'style-loader!angular2-toaster/toaster.css';
// import {
//   NbComponentStatus,
//   NbGlobalLogicalPosition,
//   NbGlobalPhysicalPosition,
//   NbGlobalPosition,
//   NbToastrService,
// } from '@nebular/theme';


@Injectable({ providedIn: 'root' })
export class ToastService {
    private subject = new Subject<any>();
    // private keepAfterRouteChange = false;

    // index = 1;
    // destroyByClick = true;
    // duration = 3000;
    // hasIcon = true;
    // position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
    // preventDuplicates = false;
    // status: NbComponentStatus = 'primary';
  
    // title = 'HI there!';
    // content = `Pins loaded now!`;
  
    // types: NbComponentStatus[] = [
    //   'primary',
    //   'success',
    //   'info',
    //   'warning',
    //   'danger',
    // ];
    constructor(
        private router: Router,
        // private toastrService: NbToastrService
        ) {
        
    }

    // public showToast(type: NbComponentStatus, title: string, body: string) {
    //   const config = {
    //     status: type,
    //     destroyByClick: this.destroyByClick,
    //     duration: this.duration,
    //     hasIcon: this.hasIcon,
    //     position: this.position,
    //     preventDuplicates: this.preventDuplicates,
    //   };
    //   const titleContent = title ? `${title}` : '';
  
    //   this.index += 1;
    //   this.toastrService.show(
    //     body,
    //     `${titleContent}`,
    //     config);
    // }
}