import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastService } from '../component/toast/toast.service';
import { AuthenticationService } from '../_services';

@Injectable()
export class UtilityProvider {
    plan: any;

  constructor(
      public http: HttpClient,
      public toastService: ToastService,
      private authenticationService: AuthenticationService
      ) {
      
        this.authenticationService.currentUserPlan.subscribe(data => {
        //   this.plan = user.data.user;
          this.plan = JSON.parse(data.features);
          console.log('User value', this.plan);

        });

    }
    
    hasPermission(id){
        // console.log(id)
        id = JSON.stringify(id);
        // console.log(this.plan.includes(id));
        // console.log(this.plan);
        return this.plan.includes(id);
    }

    showToast(type,msg) {
        this.toastService.show('I am a standard toast');

        console.log(type)
        if(type == 'success'){
            console.log(msg)

            this.toastService.show(msg, { classname: 'bg-success text-light', delay: 10000 });
        }else if(type == 'danger'){
            this.toastService.show(msg, { classname: 'bg-danger text-light', delay: 10000 });
        }else if(type == 'warning'){
            this.toastService.show(msg, { classname: 'bg-warning text-light', delay: 10000 });
        }else if(type == 'info'){
            this.toastService.show(msg, { classname: 'bg-info text-light', delay: 10000 });
        }
	}

    
    returnCurrencySymbol(c){
        let sym = '';

        if(c == 'NGN'){
            sym = '₦';
        }else if(c == 'USD'){
            sym = '$';
        }

        return sym;
    }
    
    numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}
