import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { RouterModule } from '@angular/router';
import { PublicRoutes } from './public-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChoosePlanComponent } from './choose-plan/choose-plan.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [SigninComponent, SignupComponent, ChoosePlanComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    RouterModule.forChild(PublicRoutes),
  ]
})
export class PublicModule { }
