import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { ChoosePlanComponent } from './choose-plan/choose-plan.component';
import { AuthGuard } from '../_helpers';


export const PublicRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'login',
				component: SigninComponent,
				data: {
					title: 'Login'
				}
			},
			{
				path: 'signup',
				component: SignupComponent,
				data: {
					title: 'Signup',
				}
			},
			{
				path: 'choose-plan',
				component: ChoosePlanComponent,
				// canActivate: [AuthGuard],
				data: {
					title: 'Choose Plan',
				}
			}
		]
	}
];
