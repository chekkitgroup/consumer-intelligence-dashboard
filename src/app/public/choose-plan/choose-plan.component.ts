import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { RestService} from '../../_services';
import { ViewChild, ElementRef } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Component({
  selector: 'app-choose-plan',
  templateUrl: './choose-plan.component.html',
  styleUrls: ['./choose-plan.component.css']
})
export class ChoosePlanComponent implements OnInit {
  // plans = [
  //   {
  //     id: 1,
  //     name: 'Single Core',
  //     price: '300'
  //   },
  //   {
  //     id: 2,
  //     name: 'Dual Core',
  //     price: '500'
  //   },
  //   {
  //     id: 3,
  //     name: 'Quad Core',
  //     price: '750'
  //   }
  // ]
  @ViewChild('targetBtn') targetBtn: ElementRef;

  plans = [];
  
  selectedPlan : any;

  planData = {
    planId:'',
    isTrial: false
  }
  currentUserSubscription: any;
  currentUser: any;
  processing = false;

  constructor(
    private router: Router,
    private restService: RestService,
    private authenticationService: AuthenticationService
  ) {

    if (!this.authenticationService.tokenExist()){
      this.router.navigate(['/authentication/login']);
    }


    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
   }

  ngOnInit(): void {
    // console.log(JSON.parse('["John","Peter","Sally","Jane"]'))
    this.getAllPlans();
  }

  getAllPlans() {
    this.restService.getPlans().pipe(first()).subscribe(res => {
      console.log("loyalty points", res['data']);
      if (res['data']) {
        this.plans = res['data'];
        this.plans.map(plan=>{
          if(plan.features_list){
            // console.log(plan.features_list.split(","))
            // console.log(JSON.parse(plan.features_list))
          }
        })
      }
    });
  }

  convertToArray(str){
    return str?str.split(","):[]
  }

  choosePlan(plan,isTrial=false){
    this.selectedPlan = plan;
    this.planData.planId = plan.id;
    this.planData.isTrial = isTrial;
    // this.targetBtn.nativeElement.scrollIntoView();
    // document.querySelector('#targetBtn').scrollIntoView({ behavior: 'smooth', block: 'center' });


  }

  isPlanChosen(id){
    // return 0;
    return (this.selectedPlan  && (this.selectedPlan.id == id))?true:false;
  }

  submitPlan(){
    this.processing = true;

    this.restService.selectPlan(this.planData).pipe(first()).subscribe(res => {
      console.log("chekkitUserPlan", res['data']);
      if (res['data']) {
        console.log(888)
        console.log(res['data'])
        this.updateUserPlan(res['data'].planId);
        localStorage.setItem('chekkitCurrentSubscription', JSON.stringify(res['data']));
        this.authenticationService.currentSubscriptionSubject.next(res['data']);
        // this.router.navigate(['/dashboard']);
      }
    },(err:any) =>{
      this.processing = false;
    });    
  }

  updateUserPlan(id){
    this.processing = true;
    this.restService.getPlan(id).pipe(first()).subscribe(res => {
      console.log("chekkitUserPlan", res['data']);
      if (res['data']) {
        this.processing = false;
        console.log(555)
        console.log(res['data'])
        // localStorage.setItem('chekkitUserPlan', JSON.stringify(res['data']));
        // this.authenticationService.currentUserPlanSubject.next(res['data']);
        // this.router.navigate(['/dashboard']);
      }
    },(err:any) =>{
      this.processing = false;
    });   

  }
}
