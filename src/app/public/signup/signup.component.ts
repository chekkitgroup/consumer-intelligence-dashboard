import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {


  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  recoverform = false;
  loginform = false;

  constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: [null, [Validators.required, Validators.email]],
      company_rep: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });

      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard/overview';
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }
    

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    this.loading = true;
    // this.authenticationService.login(this.f.email.value, this.f.password.value)
    //     .pipe(first())
    //     .subscribe(
    //         data => {
    //             this.bc.postMessage('Logged In');
    //             this.router.navigate([this.returnUrl]);
    //         },
    //         error => {
    //             // let err = error.json()
    //             this.error = error;
    //             console.log(error);
    //             this.loading = false;
    //         });
  }

  showRecoverForm() {
  this.loginform = !this.loginform;
  this.recoverform = !this.recoverform;
  }

}
