import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
import { SurveyRewardService, SurveyService, AuthenticationService, SurveyQuestService, AlertService } from '../../_services';
import { Subscription } from 'rxjs';
import { User } from '../../_models/user';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-airtime-reward',
  templateUrl: './airtime-reward.component.html',
  styleUrls: ['./airtime-reward.component.css']
})
export class AirtimeRewardComponent implements OnInit {
  currentUser: User;
  currentUserSubscription: Subscription;
  surveyRewardForm: FormGroup;
  loading = false;
  submitted = false;
  data: any;
  rewards: any = [];
  rewrd_num = 1;

  constructor(
    private surveyrewardServices: SurveyRewardService, 
    private formBuilder: FormBuilder,
    private surveyService: SurveyService, 
    private router: Router,
    private authenticationService: AuthenticationService,
    private surveyQuestService: SurveyQuestService,
    private alertService: AlertService) {
      
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
  }

  ngOnInit() {
    this.surveyRewardForm = this.formBuilder.group({
      reward_value: [null, [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
      reward_type: ['Airtime', [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
      reward_quant: [null, [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
    });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.surveyRewardForm.invalid);
    if (this.surveyRewardForm.invalid) {
      return;
    }
    this.loading = true;
    console.log(this.surveyRewardForm.value);
    this.updateQuestion();
    this.surveyrewardServices.createReward(this.currentUser.id, { rewards: this.rewards })
      .pipe(first())
      .subscribe(
        data => {
          console.log(data);
          if (data['status']) {
            console.log(this.data);
            this.router.navigate(['/dashboard/rewards']);
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
  updateQuestion() {
    const formData = this.surveyRewardForm.value;
    const rewardObject = {
      reward_value: formData.reward_value,
      reward_quant: formData.reward_quant,
      reward_type: formData.reward_type,
    };
    this.rewards.push(rewardObject);
  }
  addMoreReward() {
    this.updateQuestion();
    console.log('this is the question', this.rewards);
    this.surveyRewardForm.setValue({
      reward_value: '',
      reward_quant: '',
      reward_type: 'Airtime'
    });
    this.rewrd_num += 1;
  }
}
