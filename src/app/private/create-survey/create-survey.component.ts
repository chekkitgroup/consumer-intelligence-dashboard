import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
import { SurveyService } from '../../_services/survey.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../../_services/alert.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../_services/authentication.service';
import { Subscription } from 'rxjs';
import { User } from '../../_models';
import { RestService } from '../../_services/rest.service';
import { AttachSurveyUssdComponent } from '../../shared/modals/attach-survey-ussd/attach-survey-ussd.component';
import { ToastService } from '../../_services/toast.service';
import { NgbModal, ModalDismissReasons, NgbActiveModal,NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-create-survey',
  templateUrl: './create-survey.component.html',
  styleUrls: ['./create-survey.component.css']
})
export class CreateSurveyComponent implements OnInit {
  surveyForm: FormGroup;
  surveyQuestForm: FormGroup;
  questions: any = [];
  options: string[] = [null, null];
  loading = false;
  currentUserSubscription: Subscription;
  submitted = false;
  qust_num = 1;
  success_msg: any;
  currentUser: User;
  short_codes: any;
  
  constructor(private formBuilder: FormBuilder, 
    private surveyService: SurveyService,
    private alertService: AlertService, 
    private modalService: NgbModal,
    private _rest: RestService,  
    private router: Router, 
    private authenticationService: AuthenticationService) {
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user.user;
        console.log('User value', this.currentUser);
      });
  }

  ngOnInit() {
    // this.generateSurveyUssdModal(75);
    this.surveyForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      hasSurvey: ['1', Validators.required],
      question: [null, [Validators.required, Validators.maxLength(200), noWhitespaceValidator]],
      choice1: [null, [Validators.required, Validators.maxLength(200), noWhitespaceValidator]],
      choice2: [null, [Validators.required, Validators.maxLength(200), noWhitespaceValidator]],
    });
  }

  resetForm() {
    this.surveyForm.setValue({
      title: '',
      content: '',
      hasSurvey: '',
      question: '',
      choice1: '',
      choice2: ''
    });
  }

  addMoreQuestion() {
    this.updateQuestion();
    const formData = this.surveyForm.value;
    console.log('this is the question', this.questions);
    this.decreaseOptions();
    this.surveyForm.setValue({
      title: formData.title,
      content: formData.content,
      hasSurvey: formData.hasSurvey,
      question: '',
      choice1: '',
      choice2: ''
    });
    this.qust_num += 1;
  }

  updateQuestion() {
    const formData = this.surveyForm.value;
    const choices: Array<{ text: string }> = [];
    for (let index = 0; index < this.options.length; index++) {
      choices.push({ text: formData[`choice${index + 1}`] });
    }
    const questObject = {
      content: formData.question,
      choices: JSON.stringify(choices)
    };
    this.questions.push(questObject);
  }

  increaseOptions() {
    console.log(`${this.options.length}`);
    if (this.options.length < 3) {
      this.options.push(null);
      this.surveyForm.registerControl(`choice${this.options.length}`,
        new FormControl(null, [Validators.maxLength(20), noWhitespaceValidator]));
    }
  }
  get f() { return this.surveyForm.controls; }
  decreaseOptions() {
    if (this.options.length > 2) {
      console.log(`choice${this.options.length}`);
      this.surveyForm.removeControl(`choice${this.options.length}`);
      this.options.pop();

    }
  }
  createSurvey() {
    this.onSubmit();
  }
  surveyAdded(){

    this.success_msg = "Survey added successfully.";
    this.router.navigate(['/dashboard/survey']);

    // if(this.currentUser.access_level == 2){
    //   this.router.navigate(['/pages/survey']);
    //   this.generateSurveyUssdModal(data['data'].id);
    // }else{
    //   this.router.navigate(['/pages/survey']);
    // }
  }
  onSubmit() {
    this.updateQuestion();
    let surveyObjct = {
      title: this.surveyForm.value.title,
      content: this.surveyForm.value.content,
      type: this.surveyForm.value.hasSurvey
    };
    this.submitted = true;
    this.loading = true;
    console.log(this.surveyForm.value);
    this.surveyService.addSurvey({ survey: surveyObjct, question: this.questions })
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.resetForm();
          console.log("ok>>", data['data']);
          if (data['status']) {
            this.success_msg = "Survey added successfully.";
            if(this.currentUser.access_level == 2){
              this.router.navigate(['/pages/survey']);
              this.generateSurveyUssdModal(data['data'].id);
            }else{
              this.router.navigate(['/pages/survey']);
            }
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
  getShortCodes() {
    this._rest.getShortCodes().pipe(first()).subscribe(data => {
      if (data['status']) {
        this.short_codes = data['data'].short_codes;
        // this.inputData.short_code = data['data'].short_code;
        console.log(data)
      }
    });
  }
  generateSurveyUssdModal(id) {
    const modalRef = this.modalService.open(AttachSurveyUssdComponent, { size: 'lg',centered: true  });
    modalRef.componentInstance.passedData = id;
    modalRef.result.then((result) => {
      console.log(result)
      if(result){
        // this.sendUssdCode2(result)
      }
      // this._success.next("Successfully Deleted");
    }).catch((res) => {
      console.log(res)
      if(res){
        // this.sendUssdCode2(res)
      }
    });
  }
  
}
