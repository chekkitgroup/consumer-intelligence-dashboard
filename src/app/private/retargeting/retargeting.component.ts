import { Component, OnInit } from '@angular/core';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
// import { AuthenticationService } from '../../_services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
// import { RestService } from '../../_services/rest.services';
import { ProductService, AuthenticationService, SurveyService, UserService,RestService } from '../../_services';

import { first } from 'rxjs/operators';
import {UtilityProvider} from "../../_helpers/utility";
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Papa } from 'ngx-papaparse';
import 'rxjs/Rx';
import { AlertService } from '../../_services';
import { NgbModal, ModalDismissReasons, NgbActiveModal,NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AttachSurveyUssdComponent } from '../../shared/modals/attach-survey-ussd/attach-survey-ussd.component';
import { ToastService } from '../../_services/toast.service';

@Component({
  selector: 'app-retargeting',
  templateUrl: './retargeting.component.html',
  styleUrls: ['./retargeting.component.scss']
})
export class RetargetingComponent implements OnInit {
  reference = '';
  amount: any;
  allow_pay = true;
  confirm_btn = false;
  dis_form = false;
  currentUser: User;
  currentUserSubscription: Subscription;
  paymentForm: FormGroup;
  paymentData: any;
  groupName: any;
  success_msg: any;
  groupId: any;


  senderIdPayload = [];

  index = 1;
  for_pin = false;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  files: any[] = [];

  title = 'HI there!';
  content = `payment successful!`;
  uploadingCSV = true;


  // End the Closeable Alert
  // This is for the self closing alert
  private _message = new Subject<string>();

  processing = false;

  isCSV_Valid = false;
  staticAlertClosed = false;
  uploadSuccess = false;
  selectedCSVFileName = "";
  responseMessage: string;
  messageType:any;
  csvTableData:any;
  csvTableHeader:any;
  loading: boolean;
  error: any;
  submitted: boolean;
  stage = 1;
	inputData = {
		message: "",
		group_id: "",
		send_immediately: 0,
		should_save: 1,
	};
  plan: any;
  convertedFiles = [];
  planDetails: any;
  short_codes: any;
  sub: Subscription;


  
  constructor(
    private FormBuilder: FormBuilder, 
    private authenticationService: AuthenticationService,
    // private toastrService: NbToastrService, 
    private modalService: NgbModal,
    private papa: Papa,
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute,
    public utility: UtilityProvider,
    private toastService: ToastService, 
    private _rest: RestService, 
    private router: Router
    ) 
  {

    // check route for passed section
    this.sub = this.activatedRoute
      .data
      .subscribe(v =>{
        console.log(v);
        this.goToSection(v.section)
      });

    this.paymentData = this.activatedRoute.snapshot.queryParams;
    console.log('this.paymentData.type', this.paymentData);
    if (this.paymentData.type == 'pin') {
      console.log('get to pin');
      this.for_pin = true;
    }
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    // this.generateSurveyUssdModal()
  }

  
  ngOnInit() {
    this.paymentForm = this.FormBuilder.group({
      amount: [this.paymentData.amount, [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
      email: [this.paymentData.email, [Validators.required]],
    });
    this.reference = `ref-${Math.ceil(Math.random() * 10e13)}`;
    this.getSmsBalance();
    this.getShortCodes();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
// go to passed section
  goToSection(s){
    console.log(s)
    if(s == 'summary'){
      this.stage = 6
    }else if(s == 'shortcodes'){
      this.stage = 6;
      setTimeout(function () {
        console.log('hide');
        let el = document.getElementById('shortcodeSection');
        el.scrollIntoView()
      }, 500)
    }else{
      this.stage = 1
    }
  }

  createListName(){
    let data ={
      name: this.groupName
    }    


      this.processing = true;
      this._rest.createRetargetGroup(data)
          .subscribe(
              data => {
                  console.log(data);
                  this.submitted = false;
                  this.processing = false;
                    if(data['status']){
                      this.groupId =  data['data'].id;
                      this.stage = 2;
                      this.messageType = 'success';
                      // this._message.next(`${data.message}`);
                    }else{
                      this.messageType = 'danger';
                      // this._message.next(`${data.message}`);
                    }
              },
              error => {
                console.log(error);
                  this.error = error;
                  this.processing = false;
                  // this.toastService.showToast('danger','error',error);
              });

  }

  getShortCodes() {
    this._rest.getShortCodes().pipe(first()).subscribe(data => {
      if (data['status']) {
        this.short_codes = data['data'].short_codes;
        // this.inputData.short_code = data['data'].short_code;
        console.log(data)
      }
    });
  }
  generateSurveyUssdModal() {
    const modalRef = this.modalService.open(AttachSurveyUssdComponent, { size: 'lg',centered: true  });
    // modalRef.componentInstance.inputData = row;
    modalRef.result.then((result) => {
      console.log(result)
      if(result){
        this.sendUssdCode2(result)
      }
      // this._success.next("Successfully Deleted");
    }).catch((res) => {
      console.log(res)
      if(res){
        this.sendUssdCode2(res)
      }
    });
  }

  viewContacts() {
    this.router.navigate(['/pages/retargeting/contacts']);
  }
  sendUssdCode(data){
    this.stage = 1;

    this.inputData.message = 'Please dial this shortCode *347*03*' + data.short_code + '# to take our survey';
  }

  sendUssdCode2(data){
    this.stage = 1;

    this.inputData.message = 'Please dial this shortCode *347*03*' + data + '# to take our survey';
  }

  onSubmit(){
    this.submitted = true;
    this.inputData.send_immediately = this.inputData.send_immediately?1:0;
    this.inputData.group_id = this.groupId;
    this.inputData.should_save = this.inputData.should_save?1:0;
    console.log(this.inputData)
    // stop here if form is invalid
    // if (this.dataForm.invalid) {
    //     return;
    // }

      this.processing = true;
      this._rest.sendRetargetMessage(this.inputData)
          .subscribe(
              data => {
                  console.log(data);
                  this.submitted = false;
                  this.processing = false;
                  if (data['status']) {
                    this.success_msg = data['message'];
                    this.alertService.success(data['message']);
                    // this.router.navigate(['/pages/overview']);                      
                    this.stage = 6;
                  }
                  else {
                    this.success_msg = data['message'];
                    this.alertService.error(data['message']);
                  }
              },
              error => {
                console.log(error);
                  this.error = error;
                  // this.toastService.showToast('danger','error',error);
                  this.loading = false;
                  this.processing = false;
              });

  }

  
  // private showToast(type: NbComponentStatus, title: string, body: string) {
  //   const config = {
  //     status: type,
  //     destroyByClick: this.destroyByClick,
  //     duration: this.duration,
  //     hasIcon: this.hasIcon,
  //     position: this.position,
  //     preventDuplicates: this.preventDuplicates,
  //   };
  //   const titleContent = title ? `. ${title}` : '';

  //   this.index += 1;
  //   this.toastrService.show(
  //     body,
  //     `${titleContent}`,
  //     config);
  // }
  
  async cb(file){
    // let convertedFiles=[];
    // const file = this.files[0];
    let env = this;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      env.convertedFiles.push(reader.result);
      // env.senderIdPayload.passport = env.convertedFiles[0];
      // if(env.convertedFiles[1]){
      //   env.senderIdPayload.template = env.convertedFiles[1];
      // }
      console.log(env.convertedFiles)
    };
    // console.log(this.DataURIToBlob(this.files[0]))
  }
  getSmsBalance() {
    this._rest.getSmsBalance().pipe(first()).subscribe(data => {
      console.log(data['data']);
      if (data['status']) {
        this.plan = data['data'].plan;
        this.planDetails = data['data'];
        console.log(this.plan)
      }
    });
  }

  submit_bulk() {
    let formatedProducts = [];
    var t = 0;
    for(var item of this.csvTableData) {
        if(item && item[0].length > 0){

          var condition = 
          ((item[0].length > 0) 
            && (item[1].length > 0) 
            && (item[2].length > 0) );
            console.log(item[0]);
            console.log(item[1]);
            console.log(item[2]);
            console.log(condition);

          if(!condition){
            var error = "input on line " + (t+1) + " is invalid. Please review CSV and correct input.";
            // this.toastService.showToast('danger','error',error);
            this.messageType = 'danger';
            this._message.next(`${error}`);
            console.log(error);
            formatedProducts = [];
            break;
          }
          console.log(item);

          let new_item = {group_id: this.groupId,phone: item[0], name: item[1], gender: item[2], age: item[3], location: item[4], product_model: item[5], purchase_date: item[6], vip_level: parseInt(item[7]) }
          formatedProducts.push(new_item);
        }
        // console.log(item);
        console.log(formatedProducts);
        t++;
    }


      if(formatedProducts.length > 0){
        this.processing = true;
        this._rest.uploadRetargetList(formatedProducts).pipe(first()).subscribe(async data => {
          console.log('data>>>>', data);
          if (data['status']) {
            // await this.showToast(this.status, this.title, this.paymentData.type + this.content);
            this.processing = false;
            this.clearFiles()
            this.stage=3;
            // this.router.navigate(['pages/overview']);
          }
        },
        error => {
          console.log(error);
            this.error = error;
            this.processing = false;
            // this.toastService.showToast('danger','error',error);
        });
      }
  }
  clearFiles(){
    this.files = [];
  }

  uploadSenderIdScans() {
        this.processing = true;
        console.log(this.convertedFiles)
        // return
        this._rest.uploadSenderIdScans(this.convertedFiles).pipe(first()).subscribe(async data => {
          this.processing = false;
          console.log('data>>>>', data);
          if (data['status']) {
            this.stage=7;
            // await this.showToast(this.status, 'Upload successful', 'Your documents have been successfully submitted');

            // this.router.navigate(['pages/overview']);
          }
        },
        error => {
          console.log(error);
            this.error = error;
            this.processing = false;
            // this.toastService.showToast('danger','error',error);
        });
  }


  // LOAD CSV FILE FROM INPUT
  fileChangeListener($event: any): void {

    console.log($event);
      const files = $event;
      console.log(files);

      // return;

      if (files !== null && files !== undefined && files.length > 0) {
        this.selectedCSVFileName = files[0].name;

        const reader: FileReader = new FileReader();
        reader.readAsText(files[0]);
        reader.onload = e => {

          const csv = reader.result;
          const results = this.papa.parse(csv as string, { header: false });

          // VALIDATE PARSED CSV FILE
          if (results !== null && results !== undefined && results.data !== null &&
            results.data !== undefined && results.data.length > 0 && results.errors.length === 0) {
            this.isCSV_Valid = true;

            // PERFORM OPERATIONS ON PARSED CSV
             this.csvTableHeader = results.data[0];

             this.csvTableData = [...results.data.slice(1, results.data.length)];
            console.log(this.csvTableHeader);
            console.log(this.csvTableData);

          } else {
            for (let i = 0; i < results.errors.length; i++) {
              console.log( 'Error Parsing CSV File: ',results.errors[i].message);
            }
          }
        };
      } else {
        console.log('No File Selected');
      }

    }
    // https://medium.com/@tarekabdelkhalek/how-to-create-a-drag-and-drop-file-uploading-in-angular-78d9eba0b854
  /**
   * on file drop handler
   */
  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  onFileDropped2($event) {
    this.prepareFilesList2($event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  fileBrowseHandler2(files) {
    this.prepareFilesList2(files);
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile(index: number) {
    this.files.splice(index, 1);
  }

  /**
   * Simulate the upload process
   */
  uploadFilesSimulator(index: number) {
    setTimeout(() => {
      if (index === this.files.length) {
        return;
      } else {
        const progressInterval = setInterval(() => {
          if (this.files[index].progress === 100) {
            clearInterval(progressInterval);
            this.uploadFilesSimulator(index + 1);
          } else {
            this.files[index].progress += 5;
          }
        }, 200);
      }
    }, 1000);
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      // console.log(files[0].name.substr(files[0].name.lastIndexOf('.') + 1))
      if(files[0].name.substr(files[0].name.lastIndexOf('.') + 1) !== 'csv'){
        // this.toastService.showToast('danger','wrong file input','file type not accepted, please upload a csv');
        return false;
      }
      item.progress = 0;
      this.files[0] = item;
      console.log(item);
    }
    this.fileChangeListener(files)

    this.uploadFilesSimulator(0);
  }
  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
 async prepareFilesList2(files: Array<any>) {
    for (const item of files) {
      // console.log(files[0].name.substr(files[0].name.lastIndexOf('.') + 1))
      // if(files[0].name.substr(files[0].name.lastIndexOf('.') + 1) !== 'csv'){
      //   this.toastService.showToast('danger','wrong file input','file type not accepted, please upload a csv');
      //   return false;
      // }
      console.log()
      item.progress = 0;
      if(this.files.length == 2){
        this.files[1] = item;
        await this.cb(item)
      }else{
        this.files.push(item);
        await this.cb(item)
      }
      console.log(item);
    }
    // this.fileChangeListener(files)

    this.uploadFilesSimulator(0);
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }


}
