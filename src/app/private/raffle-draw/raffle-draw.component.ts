import { ChangeDetectorRef,Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { ProductService, AuthenticationService, SurveyService, UserService,AlertService } from '../../_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-raffle-draw',
  templateUrl: './raffle-draw.component.html',
  styleUrls: ['./raffle-draw.component.css']
})
export class RaffleDrawComponent implements OnInit {

  editing = {};
  rows = [];
  temp = [];
  loadingIndicator = true;
  photoSelected = false;
  closeResult: any;
  data: any;
  surveyForm: FormGroup;
  loading = false;
  uploading = false;
  adding = false;
  loyalt_points: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  user_recordCounts: any;
  raffleForm: FormGroup;
  raffleImageForm: FormGroup;
  submitted: boolean;
  success_msg: any;
  raffleData = {
    reward_value : '',
    reward_type: 'Raffle',
    point_to_claim_reward: '1',
    reward_quantity: '1',
    gifts:[]
  };

  gift = {
    photo: null,
    name: ''
  }
  currentGiftIndex = 0;
  @ViewChild('giftPhoto',{static:true}) myInputVariable: ElementRef;
  raffles: any;

  constructor(
    private router: Router, 
    private alertService: AlertService,
    private authenticationService: AuthenticationService,
    private surveyService: SurveyService,
    private formBuilder: FormBuilder, 
    private changeDetectorRef: ChangeDetectorRef,
    private userService: UserService
    ) {
      
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.getAllLoyaltyPoints();
    setTimeout(() => { this.loadingIndicator = false; }, 1500);
    this.raffleImageForm = this.formBuilder.group({
      photo: ['', Validators.required],
    });
  }
  get f() { return this.raffleImageForm.controls; }

  ngOnInit() {
    this.raffleForm = this.formBuilder.group({
      reward_value: ['er', Validators.required],
      reward_type: ['Raffle', Validators.required],
      point_to_claim_reward: ['1', Validators.required],
      reward_quantity: ['1', Validators.required]
      // userId: ['', Validators.required],
    });
  }
  getAllLoyaltyPoints() {
    this.surveyService.getAllUserRaffle(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log("loyalty points", data['data'].raffles);
      if (data['data'].raffles) {
        this.raffles = data['data'].raffles;
      }
    });
  }

  
  async onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      // console.log();
      this.getBase64(file)
      this.raffleImageForm.get('photo').setValue(file);

      // this.gift.photo = await this.getBase64(file);
    this.uploading = true;
    // console.log(this.productForm.value);
    const formData = new FormData();
    formData.append('photo', this.raffleImageForm.get('photo').value);
    // console.log(this.raffleImageForm.get('photo').value)
    console.log(formData)
   


    this.surveyService.uploadGiftImage(formData)
      .pipe(first())
      .subscribe(
        data => {
          console.log('ok>>', data);
          this.uploading = false;
          if (data) {
            this.gift.photo = data['data'];
            event.srcElement.value = null;
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.uploading = false;
        });

      console.log(this.gift.photo)
      this.photoSelected = true;

      this.changeDetectorRef.detectChanges();
  
    }
  }

  // onSubmit() {
  // }
  
  ViewRaffle(d) {
    console.log("product", d);
    let data = JSON.stringify(d.id);
    this.router.navigate(['/dashboard/rewards/view-raffle'], { queryParams: { data } });
  }
  resetForm(){
    this.raffleData = {
      reward_value : '',
      reward_type: 'Raffle',
      point_to_claim_reward: '1',
      reward_quantity: '1',
      gifts:[]
    };
  
    this.gift = {
      photo: null,
      name: ''
    }
    this.raffleImageForm.get('photo').setValue(null);
  }

  getBase64(file) {
    let env = this;
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      // console.log(reader.result);
      // env.gift.photo = reader.result;
      return reader.result;
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
 }
 switchToGift(i) {
   this.gift = this.raffleData.gifts[i];

   this.currentGiftIndex = i;
 }
 saveGift(){
   console.log(this.gift)

  //  this.raffleData.gifts.push(this.gift);
   this.raffleData.gifts[this.currentGiftIndex] = this.gift;

   
   console.log(this.raffleData)

   this.changeDetectorRef.detectChanges();


 }

 addGift(){
   console.log(this.gift)
   this.saveGift();
   
   this.gift = {
    photo: null,
    name: ''
  }
  this.photoSelected = false;
  
  this.raffleData.gifts.push(this.gift);

  this.currentGiftIndex +=1;

   console.log(this.raffleData)
   this.changeDetectorRef.detectChanges();


 }

  toggleAdding() {
    this.adding = !this.adding;
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    console.log(this.raffleData);
    this.surveyService.addRaffleDraw(this.raffleData)
      .pipe(first())
      .subscribe(
        data => {
          console.log("ok>>", data['data']);
          if (data['status']) {
            this.resetForm()
            this.getAllLoyaltyPoints();
            this.adding = false;
            this.loading = false;

            // this.router.navigate(['/pages/loyalty-points']);
          }
          else {
            this.success_msg = data['message'];
            this.loading = false;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  deleteRaffle(id) {
    this.surveyService.deleteRaffle(id)
      .pipe(first())
      .subscribe(
        data => {
          console.log("ok>>", data['data']);
          if (data['status']) {
            this.resetForm()
            this.getAllLoyaltyPoints();
            this.adding = false;
            // this.router.navigate(['/pages/loyalty-points']);
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
