import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { ProductService } from '../../_services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SurveyRewardService } from '../../_services/surveyReward.service';
import { SurveyService } from '../../_services/survey.service';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-create-batch',
  templateUrl: './create-batch.component.html',
  styleUrls: ['./create-batch.component.css']
})
export class CreateBatchComponent implements OnInit {
  success_msg: any;
  loadingIndicator = true;
  closeResult: any;
  data: any;
  subProductForm: FormGroup;
  loading = false;
  submitted = false;
  productObject: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  rows: any;
  temp: any[];
  alertService: any;
  save_survey: any;
  save_surveyReward: any;
  loyalty_points: any;
  navData: any;
  slug: string;


  loyalty = false;
  others = true;



  constructor(
    private productService: ProductService, 
    private activatedRoute: ActivatedRoute,
    private surveyService: SurveyService, 
    private formBuilder: FormBuilder,
    private router: Router,
    private surveyrewardServices: SurveyRewardService, 
    private authenticationService: AuthenticationService
    ) {

      // this.productObject = JSON.parse(this.activatedRoute.snapshot.queryParams.data);
     
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user.user;
        console.log('User value', this.currentUser);
      });

      this.slug = this.activatedRoute.snapshot.queryParams.q;
      console.log('product', (this.navData));
      this.getProduct();

      this.subProductForm = this.formBuilder.group({
        productId: [''],
        product_name: ['', [Validators.required]],
        expiry_date: ['', Validators.required],
        production_date: ['', Validators.required],
        survey_id: ['', Validators.required],
        reward_id: ['', Validators.required],
      });

     }

     ngOnInit() {
      this.productService.productDetails$.subscribe(
        (details) => {
          this.productObject = details;
          console.log(details)
          if(this.productObject){
            this.subProductForm.get('productId').setValue(this.productObject.id);
          }
        }
      );
    }
    onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      console.log(this.subProductForm.invalid);
      if (this.subProductForm.invalid) {
        return;
      }
      this.loading = true;
      console.log(this.subProductForm.value);
      this.productService.create_batch(this.subProductForm.value)
        .pipe(first())
        .subscribe(
          dataRslt => {
            console.log(dataRslt)
            if (dataRslt['status']) {
              const data = JSON.stringify({
                batch: dataRslt['data'],
                product: this.productObject,
              });
              this.router.navigate(['/dashboard/products'], { queryParams: { data }, skipLocationChange: true });
            } else {
              this.success_msg = dataRslt['message'];
            }
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          });
    }
    getAllSurveys() {
      this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
        console.log('result of survey', data['data']);
        if (data['data'].surveys) {
          console.log('okay ooo', data['data'].surveys);
          this.data = data['data'].surveys;
          this.save_survey = this.data;
          localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
        }
      });
    }
    getAllSurveyRewards() {
      this.surveyrewardServices.getAllSurveyReward(this.currentUser.id).pipe(first()).subscribe(data => {
        console.log('result of reward', data['data']);
        if (data['data'].rewards) {
          this.data = data['data'].rewards;
          this.save_surveyReward = this.data;
          console.log('rewards', this.save_surveyReward);
          localStorage.setItem('allsurveyRewards', JSON.stringify(data['data'].rewards));
        }
      });
    }
  
  
    getProduct() {
      this.productService.getProduct(this.slug).pipe(first()).subscribe(data => {
        console.log("result of survey", data['data']);
        if (data['data']) {
          console.log('okay ooo', data['data']);
          this.productObject = data['data'];
          this.subProductForm.get('productId').setValue(this.productObject.id);

          this.getAllSurveys();
          this.getAllSurveyRewards();
          this.getAllLoyaltyPoints();
        }
      });
    }

    getAllLoyaltyPoints() {
      this.surveyService.getAllUserLoyaltyPoints(this.currentUser.id).pipe(first()).subscribe(data => {
        console.log("loyalty points", data['data'].loyaltypoints);
        if (data['data'].loyaltypoints) {
          this.loyalty_points = data['data'].loyaltypoints;
        }
      });
    }
  
    onChange(evnt: any) {
      console.log('yeah', evnt.target.value);
      if (evnt.target.value == 'Loyalty Point') {
        this.loyalty = false;
        this.others = true;
      } else if (evnt.target.value == 'Airtime/Merchandize') {
        this.loyalty = true;
        this.others = false;
      }
    }
}
