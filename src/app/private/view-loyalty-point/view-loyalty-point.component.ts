import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SurveyService, AlertService } from '../../_services';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-view-loyalty-point',
  templateUrl: './view-loyalty-point.component.html',
  styleUrls: ['./view-loyalty-point.component.css']
})
export class ViewLoyaltyPointComponent implements OnInit {
  loyaltyPointForm: FormGroup;
  loading = false;
  submitted = false;
  success_msg: any;
  loyaltyPoint_data: any;

  constructor(
    private formBuilder: FormBuilder, 
    private surveyService: SurveyService, 
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService, 
    private router: Router) 
    {
      this.loyaltyPoint_data = this.activatedRoute.snapshot.queryParams;
    }


    ngOnInit() {
      this.loyaltyPointForm = this.formBuilder.group({
        loyalty_name: [this.loyaltyPoint_data.loyalty_name, Validators.required],
        scan_point_to_allocate: [this.loyaltyPoint_data.scan_point_to_allocate, Validators.required],
        loyalty_reward: [this.loyaltyPoint_data.loyalty_reward, Validators.required],
        loyalty_reward_value: [this.loyaltyPoint_data.loyalty_reward_value, Validators.required],
        loyalty_point_convert: [this.loyaltyPoint_data.loyalty_point_convert, Validators.required],
      });
    }
    onSubmit() {
      this.submitted = true;
      this.loading = true;
      console.log(this.loyaltyPointForm.value);
      this.surveyService.updateLoyalty(this.loyaltyPoint_data.slug, this.loyaltyPointForm.value)
        .pipe(first())
        .subscribe(
          data => {
            console.log("ok>>", data['data']);
            if (data['status']) {
              this.router.navigate(['/dashboard/rewards/loyalty-points']);
            }
            else {
              this.success_msg = data['message'];
            }
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          });
    }
}
