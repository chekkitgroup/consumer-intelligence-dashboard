import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Survey } from '../../_models/survey';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
import { SurveyService } from '../../_services/survey.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../../_services/alert.service';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'ngx-edit-survey',
  templateUrl: './edit-survey.component.html',
  styleUrls: ['./edit-survey.component.scss'],
})
export class EditSurveyComponent implements OnInit {
  survey_data: any;
  questions: any;
  survey: any;
  surveyForm: FormGroup;
  surveyQuestForm: FormGroup;
  options: string[] = [null, null];
  loading = false;
  submitted = false;
  qust_num = 1;
  success_msg: any;
  newchoices: any;
  numbers: number[];

  constructor(
    private activatedRoute: ActivatedRoute, 
    private formBuilder: FormBuilder,
    private surveyService: SurveyService, 
    private alertService: AlertService, 
    private router: Router,
    private authenticationService: AuthenticationService
    ) {
    // if (!authenticationService.tokenExist()) {
    //   router.navigate(['pages/auth/login']);
    // }
  }

  ngOnInit() {
    console.log(this.activatedRoute.snapshot);
    this.survey_data = JSON.parse(this.activatedRoute.snapshot.queryParams.data);
    this.survey = this.survey_data.survey;
    // this.questions = this.survey_data.question;
    this.surveyForm = this.formBuilder.group({
      title: [this.survey.title, Validators.required],
      content: [this.survey.content, Validators.required],
      question: ['', [Validators.required, Validators.maxLength(100), noWhitespaceValidator]],
      choice1: ['', [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
      // choice2: ['', [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
    });
    console.log(this.survey_data)
    this.getSurveyQuestionsWithResponses();
    return;

    let questions = this.questions;
    const numbers=Array(questions.length).fill(0).map((_, idx) =>  idx + 1 )
    console.log(numbers)

  }
  get f() { return this.surveyForm.controls; }
  addMoreQuestion(n) {
    console.log(this.questions[n])
    let q = this.questions[n];
    this.updateQuestion();
    const formData = this.surveyForm.value;
    console.log('this is the question', this.questions);
    this.decreaseOptions();
    this.surveyForm.setValue({
      title: formData.title,
      content: formData.content,
      question: q.content,
      choice1: '',
      choice2: '',
    });
    this.qust_num += 1;
  }



  editQuestion(i){

    // this.surveyForm.removeControl(`choice1`);
    this.surveyForm.removeControl(`choice2`);
    this.surveyForm.removeControl(`choice3`);
    this.surveyForm.removeControl(`choice4`);
    
    this.options = [null];

    console.log(this.options)
    console.log(this.surveyForm)

    let choices = JSON.parse(this.questions[i].choices);

    console.log(choices)
    const formData = this.surveyForm.value;

    
    this.surveyForm.setValue({
      title: formData.title,
      // type: this.survey_data.type,
      content: formData.content,
      question: this.questions[i].content,
      choice1: choices[0].text,
      // choice2: choices[1].text,
      // choice4: '',
    });

    if(choices[1] && choices[1].text){
      // console.log('<<<choices[2].text>>>')
      // console.log(choices[1].text)
      // this.options.push(null);
      // this.surveyForm.registerControl(`choice2`,new FormControl(choices[1].text, [Validators.maxLength(20), noWhitespaceValidator]));

      // this.surveyForm.setValue({
      //   title: formData.title,
      //   // type: this.survey_data.type,
      //   content: formData.content,
      //   question: this.questions[i].content,
      //   choice1: choices[0].text,
      //   choice2: choices[1].text,
      //   // choice3: '',
      //   // choice4: '',
      // });
    }

    

    if(choices[1] && choices[1].text){
      console.log('<<<choices[2].text>>>')
      // this.surveyForm.removeControl(`choice2`);
      this.options.push(null);
      console.log(this.options)
      this.surveyForm.registerControl(`choice2`,
        new FormControl(choices[1].text, [Validators.maxLength(20), noWhitespaceValidator]));

        console.log(this.surveyForm)
    }

    if(choices[2] && choices[2].text){
      console.log('<<<choices[3].text>>>')
      // this.surveyForm.removeControl(`choice3`);
      this.options.push(null);
      this.surveyForm.registerControl(`choice3`,
        new FormControl(choices[2].text, [Validators.maxLength(20), noWhitespaceValidator]));
    }
    return

    if(choices[3] && choices[3].text){
      console.log('<<<choices[4].text>>>')
      this.surveyForm.removeControl(`choice4`);
      this.options.push(null);
      this.surveyForm.registerControl(`choice4`,
        new FormControl(choices[3].text, [Validators.maxLength(20), noWhitespaceValidator]));
    }

  }

  addMoreQuestsion(){
    
  }
  updateQuestion() {
    const formData = this.surveyForm.value;
    const choices: Array<{ text: string }> = [];
    for (let index = 0; index < this.options.length; index++) {
      choices.push({ text: formData[`choice${index + 1}`] });
    }
    const questObject = {
      content: formData.question,
      choices: JSON.stringify(choices),
    };
    this.questions.push(questObject);
  }
  increaseOptions() {
    console.log(`${this.options.length}`);
    if (this.options.length < 3) {
      this.options.push(null);
      this.surveyForm.registerControl(`choice${this.options.length}`,
        new FormControl(null, [Validators.maxLength(20), noWhitespaceValidator]));
    }
  }

  decreaseOptions() {
    if (this.options.length > 2) {
      console.log(`choice${this.options.length}`);
      this.surveyForm.removeControl(`choice${this.options.length}`);
      this.options.pop();
    }
  }
  createSurvey() {
    this.onSubmit();
  }
  onSubmit() {
    const surveyObjct = {
      title: this.surveyForm.value.title,
      content: this.surveyForm.value.content,
    };
    this.submitted = true;
    this.loading = true;
    console.log(this.surveyForm.value);
    this.surveyService.updateSurvey(this.survey_data.survey.slug, { survey: surveyObjct, question: this.questions })
      .pipe(first())
      .subscribe(
        data => {
          console.log('\'ok>>\'', data['data']);
          if (data['status']) {
            this.success_msg = 'Survey added successfully.';
          } else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
  toJson(options: any) {
    return JSON.parse(options);
  }


  
  getSurveyQuestionsWithResponses() {
    this.surveyService.getSurveyQuestionsWithResponses(this.survey_data.survey.id).subscribe(data => {
      // console.log(data['data']);
      if (data['status']) {
        // let uniquePhoneNumbers = responses.map(item => item.phone_number)
        this.questions  = data['data'].questions;
        console.log(this.questions);
        this.prepareQuestions();
        // let response  = data.json().data.responses;
      }
    });
  }

  prepareQuestions(){

    let questions = this.questions;
    const numbers=Array(questions.length).fill(0).map((_, idx) =>  idx + 1 );
    this.numbers = numbers;

    this.editQuestion(0)
    
    // numbers.forEach((num)=>{
    //   let i = num-1;
    //   const fc = this.formBuilder.group({
    //     title: [this.survey.title, Validators.required],
    //     content: [this.survey.content, Validators.required],
    //     question: [questions[i], [Validators.required, Validators.maxLength(100), noWhitespaceValidator]],
    //     choice1: ['', [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
    //     choice2: ['', [Validators.required, Validators.maxLength(20), noWhitespaceValidator]],
    //   });
    //   this.surveyForm.addControl(num.toString(),fc)
    // })

    console.log(this.surveyForm)

  }
}
