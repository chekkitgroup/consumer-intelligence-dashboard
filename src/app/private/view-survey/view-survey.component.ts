import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Survey } from '../../_models/survey';
import { SurveyService } from '../../_services/survey.service';
import { first } from 'rxjs/operators';
import { Survey_Question } from '../../_models/survey_quest';
import { ExportToCsv } from 'export-to-csv';
import * as c3 from 'c3';

@Component({
  selector: 'ngx-view-survey',
  templateUrl: './view-survey.component.html',
  styleUrls: ['./view-survey.component.scss']
})
export class ViewSurveyComponent implements OnInit {
  survey_data: any;
  questions: any;
  data: any;
  options: any;
  Colour : any;
  theme: any;

  // themeSubscription: any;
  rows: any[];
  data2: { labels: string[]; datasets: { data: number[]; label: string; backgroundColor: string; }[]; };
  options2: any;
  options3: any;
  data3: { labels: string[]; datasets: { data: any[]; backgroundColor: any[]; }[]; };
  rows2: any[];
  chartColors: any[];
  surveyWithAnalytics: any;
  questionsWithResponses: any;
  questionsWithResponses2: any;
  // Chart
  public chartLabels:  any;
  public chartData : any;
  public chartType = 'pie';

  public chartLabels2:  any;
  public chartData2 : any;
  public chartType2 = 'bar';

  public chartLabels3:  any;
  public chartData3 : any;
  public chartType3 = 'doughnut';


  public chartLabels4:  any;
  public chartData4 : any;
  public chartType4 = 'pie';

  colors1 = [{backgroundColor: ['#00897b', '#757575', '#039be5']}];
  colors2 = [{backgroundColor: ['#424242', '#1e88e5', '#ffa000']}];
  colors3 = [{backgroundColor: ['#e53935', '#3949ab', '#757575']}];

  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private surveyService: SurveyService) {
  }

  ngOnInit() {
    console.log(this.activatedRoute.snapshot.queryParams);
    this.survey_data = this.activatedRoute.snapshot.queryParams;
    console.log(this.survey_data);
    this.getSurveyQuestions();
    if(this.survey_data.type == 1){
      this.getLabelSurveyQuestionsWithResponses();
    }else{
      this.getSurveyQuestionsWithResponses();
    }

    this.setChartConfig()
  }
  ionViewDidLoad() {
  }
  ngAfterViewInit() {
    // let chart = c3.generate({
    // bindto: '#chart',
    //     data: {
    //         columns: [
    //             ['data1', 30, 200, 100, 400, 150, 250],
    //             ['data2', 50, 20, 10, 40, 15, 25]
    //         ]
    //     }
    // });
}
  plotPieChart(m,f){
    let male;
    let female;

    this.Colour = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;
      console.log("datas", male, female);
      // this.data = {
      //   labels: ['Male', 'Female'],
      //   datasets: [{
      //     data: [male || m, female || f],
      //     backgroundColor: [colors.primaryLight, colors.dangerLight],
      //   }],
      // };

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  setChartConfig(){
    this.Colour = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;
      let Labels = ['5 - 19', '20 - 29', '30 - 39', 'Above 40'];

      // this.data2 = {
      //   labels: Labels,
      //   datasets: [{
      //     data: [opt1,opt2,opt3,opt4],
      //     label: 'Age Group',
      //     backgroundColor: [colors.primaryLight, colors.dangerLight, colors.successLight],
      //   }],
      // };
    this.chartColors = [colors.primaryLight, colors.dangerLight, colors.successLight];
    
      this.options2 = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                display: false,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
          yAxes: [
            {
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
        },
      };
    });

  //   var chart = c3.generate({
  //     bindto: "#tech_chart",
  //     axis: {
  //         y: {
  //             tick: {
  //                 format: function (d) {
  //                     return (parseInt(d) == d) ? d : null;
  //                 }
  //             }
  //         }
  //     }
  // });

  }

  plotPieChart2(opt1,opt2,opt3){
    this.Colour = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;
      // console.log("datas", male, female);
      this.data3 = {
        labels: ['agege', 'ikoyi', 'ogba'],
        datasets: [{
          data: [opt1 , opt2 , opt3],
          backgroundColor: [colors.primaryLight, colors.dangerLight, colors.successLight],
        }],
      };

      this.options3 = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        }, 
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  plotBarChart(opt1,opt2,opt3,opt4){
    this.Colour = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;
      let Labels = ['5 - 19', '20 - 29', '30 - 39', 'Above 40'];

      this.data2 = {
        labels: Labels,
        datasets: [{
          data: [opt1,opt2,opt3,opt4],
          label: 'Age Group',
          backgroundColor: this.Colour.hexToRgbA(colors.primaryLight, 0.8),
        }],
      };

      this.options2 = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                display: false,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
          yAxes: [
            {
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
        },
      };
    });
  }

  getSurveyQuestions() {
    this.surveyService.getSurveyQuestions(this.survey_data.id).pipe(first()).subscribe(data => {
      console.log(data['data']);
      if (data['status']) {
        this.questions = data['data'];
        // localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
      }
    });
  }
  
  getSurveyQuestionsWithResponses() {
    this.surveyService.getSurveyQuestionsWithResponses(this.survey_data.id).pipe(first()).subscribe(data => {
      // console.log(data['data']);
      if (data['status']) {
        let response = data['data'].responses;
        // filter fixed responses from api response
        var fixedResponses =  response.filter(function(res) {
          return res.type == 2;
        });
        
        var dynamicResponses =  response.filter(function(res) {
          return res.type == 1;
        });
        
        this.rows = this.processFixedResponses(fixedResponses);
        this.rows2 = this.processDynamicResponses(dynamicResponses);

        // filter fixed responses 1
        var fixedResponses1 =  fixedResponses.filter(function(res) {
          return res.questionNumber == 1;
        });

        // filter fixed responses 2
        var fixedResponses2 =  fixedResponses.filter(function(res) {
          return res.questionNumber == 2;
        });
        
        // 
        // return data['data'];
        // console.log(fixedResponses2)
        // localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
      }
    });
  }

  getLabelSurveyQuestionsWithResponses() {
    this.surveyService.getLabelSurveyQuestionsWithResponses(this.survey_data.id).pipe(first()).subscribe(data => {
      // console.log(data['data']);
      if (data['status']) {
        let response = data['data'].responses;

        // console.log(data['data'])

        let questionsWithResponses = data['data'].questions;
        this.questionsWithResponses = questionsWithResponses;

        this.questionsWithResponses2 = data['data'].surveyResponses;
        console.log(this.questionsWithResponses)

        let env = this;
        let qRes = questionsWithResponses;
        let qRes2 = this.questionsWithResponses2;
        let i = 0;

        qRes.forEach(function (entry) {
          // console.log('<<<eeeee>>>')
          let ff = env.returnFilteredResponses(entry.id, response)
          questionsWithResponses[i].responses = ff;

          var resFlat = questionsWithResponses[i].responses .map(function (r) {
            return r.choice
          });

          questionsWithResponses[i].responsesStats = env.returnRepeatedArrayValues(resFlat);
          var d = questionsWithResponses[i].responsesStats;
          questionsWithResponses[i].labels = Object.keys(d);
          questionsWithResponses[i].values = Object.values(d);

          let data = {
            labels: Object.keys(d),
            datasets: [{
              data: Object.values(d),
              backgroundColor: env.chartColors,
            }],
          };
          questionsWithResponses[i].chartData = data;

          // console.log(Object.keys(d))
          // console.log(questionsWithResponses[i].responsesStats)

          i++;
        })

        console.log('<111111kkkkk111111>')
        console.log(questionsWithResponses)
        i = 0;

        qRes2.forEach(function (entry) {
          // console.log('<<<eeeee>>>')
          // let ff = env.returnFilteredResponses(entry.id, response)
          // questionsWithResponses[i].responses = ff;
          // let pp = surveyResponses

          var resFlat = env.questionsWithResponses2.map(function (r) {
            return r.res
          });

          // console.log('<<<resFlat>>>')
          // console.log(resFlat)
          // questionsWithResponses[i].responsesStats = env.returnRepeatedArrayValues(resFlat);
          // var d = questionsWithResponses[i].responsesStats;
          env.questionsWithResponses2[i].labels = Object.keys(entry.res);
          env.questionsWithResponses2[i].values = Object.values(entry.res);
          env.questionsWithResponses2[i].values.push(0)

          let data = {
            labels: Object.keys(resFlat),
            datasets: [{
              data: Object.values(resFlat),
              backgroundColor: env.chartColors,
            }],
          };
          env.questionsWithResponses2[i].chartData = data;

          // console.log(Object.keys(d))
          // console.log(questionsWithResponses[i].responsesStats)

          i++;
        })


        this.surveyWithAnalytics = qRes;
        console.log('<<<resFlddddat>>>')
        console.log(env.questionsWithResponses2)

        // filter fixed responses from api response
        // var fixedResponses =  response.filter(function(res) {
        //   return res.type == 2;
        // });
        
        // var dynamicResponses =  response.filter(function(res) {
        //   return res.type == 1;
        // });
        
        // this.rows = this.processFixedResponses(fixedResponses);
        // this.rows2 = this.processDynamicResponses(dynamicResponses);

        // // filter fixed responses 1
        // var fixedResponses1 =  fixedResponses.filter(function(res) {
        //   return res.questionNumber == 1;
        // });

        // // filter fixed responses 2
        // var fixedResponses2 =  fixedResponses.filter(function(res) {
        //   return res.questionNumber == 2;
        // });
        
        // 
        // return data['data'];
        // console.log(fixedResponses2)
        // localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
      }
    });
  }

  returnRepeatedArrayValues(a) {
    var result = { };
    for(var i = 0; i < a.length; ++i) {
      if(!result[a[i]])
          result[a[i]] = 0;
      ++result[a[i]];
    }

    return result;
  }

  returnFilteredResponses(id,res) {
    // console.log(id)
    // console.log(res)
        var tr = res.filter(function(resp) {
          return resp.surveysQuestionId == id;
        });

        // console.log(tr)
    return tr;
  }

  processFixedResponses(responses: any) {
    let uniquePhoneNumbers = responses.map(item => item.phone_number)
                              .filter((value, index, self) => self.indexOf(value) === index);

    let userResponses = [];
    let surveyOne = [];
    let surveyTwo = [];

    var env = this;
    uniquePhoneNumbers.forEach(function (entry) {
      let resp = { 'phone': entry, 'answer1':'', 'answer2':'', 'answer3':''}
      var n1 =  env.returnAnswer("1",entry, responses);
      var n2 =  env.returnAnswer("2",entry, responses);
      // console.log(n1)
      resp.answer1 = n1;
      resp.answer2 = n2;
      surveyOne.push(n1)
      surveyTwo.push(n2)
      userResponses.push(resp)
      // let resp = { 'phone': entry, 'answerGender':n1}
    });

    console.log(surveyTwo)
    this.returnUssdFirstStats(surveyOne);
    this.returnUssdSecondStats(surveyTwo);
    // this.returnUssdFirstStats(surveyOne);
    return(userResponses)
  }

  processDynamicResponses(responses: any) {
    console.log(responses);
    let uniquePhoneNumbers = responses.map(item => item.phone_number)
                              .filter((value, index, self) => self.indexOf(value) === index);

    let userResponses = [];
    let surveyThree = [];

    var env = this;
    uniquePhoneNumbers.forEach(function (entry) {
      let resp = { 'phone': entry, 'answer1':'', 'answer2':'', 'answer3':''}
      var n3 =  env.returnAnswer("3",entry, responses);
      // console.log(n1)
      resp.answer1 = n3;

      surveyThree.push(n3)
      // userResponses.push(resp)
      // let resp = { 'phone': entry, 'answerGender':n1}
    });

    console.log(surveyThree)
    this.returnUssdThirdStats(surveyThree);
    // this.returnUssdFirstStats(surveyOne);
    return(userResponses)
  }

  generateExcel(action = 'open') {
    var ds = this.rows;

    console.log(ds);


    var d = new Date();
    var n = d.getTime();
    let newFileName = 'chekkit-pin-sheet-' + n;

    const options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Chekkit Exported Survey CSV',
      useTextFile: false,
      useBom: true,
      filename: newFileName,
      useKeysAsHeaders: false,
      headers: ['Phone Number', 'Gender', 'Age Group', 'Location']
    };

   

      const csvExporter = new ExportToCsv(options);
      
      csvExporter.generateCsv(ds);    
  }

  returnAnsPercentage(a,arr) {
    var sum = arr.reduce(function(a, b) { return a + b; }, 0);
    return (a/sum) * 100;
  }

  returnAnswer(num, phone, res) {
    let ans = res.filter(function(res) {
      return res.questionNumber == num && res.phone_number == phone;
    });
    return ans[0]?ans[0].answer:'';
  }

  // get first USSD survey stats
  returnUssdFirstStats(d){
    var maleRes = this.returnAnswerResponseOccurence('male', d);
    var femaleRes = this.returnAnswerResponseOccurence('female', d);
    this.plotPieChart(maleRes,femaleRes);
    // console.log(maleRes)
  }
  // let response = `CON What's your Age Group \n 1. 5 - 19 years \n 2. 20 - 29 years \n 3. 30 - 39 years \n 4. Above 40 years`;

  // get first USSD survey stats
  returnUssdSecondStats(d){
    var option1 = this.returnAnswerResponseOccurence('5 - 19', d);
    var option2 = this.returnAnswerResponseOccurence('20 - 29', d);
    var option3 = this.returnAnswerResponseOccurence('30 - 39', d);
    var option4 = this.returnAnswerResponseOccurence('Above 40', d);
    this.plotBarChart(option1,option2,option3,option4);
    console.log(option2)
  }

  // get first USSD survey stats
  returnUssdThirdStats(d){
    var option1 = this.returnAnswerResponseOccurence('agege', d);
    var option2 = this.returnAnswerResponseOccurence('ogba', d);
    var option3 = this.returnAnswerResponseOccurence('ikoyi', d);
    this.plotPieChart2(option1,option2,option3);
    console.log(option2)
  }

  // return number of times an answer occurs in an array
  returnAnswerResponseOccurence(ans, obj) {
    let flt =[];
    flt = obj.filter(function(res) {
      return res == ans;
    });
    return flt.length;
  }

  toJson(options: any) {
    return JSON.parse(options);
  }

  gotoEditSurvey(surveyData: Survey, questions: Survey_Question) {
    console.log('yeshshs ', surveyData);

    let data = JSON.stringify({
      survey: surveyData,
      question: questions
    });

    this.router.navigate(['/dashboard/edit-survey'], { queryParams: { data }, skipLocationChange: true });

  }
}
