import { Component, ViewChild, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService, SurveyQuestService, SurveyService, AlertService, SurveyRewardService } from '../../_services';
import { first } from 'rxjs/operators';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { UtilityProvider } from '../../_helpers/utility';

@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.css']
})
export class RewardsComponent implements OnInit {
  options: string[] = [null];
  editing = {};
  rows = [];
  temp = [];
  success_msg: any;
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  closeResult: any;
  data: any;
  columns = [
    { prop: 'Question' },
    { name: 'Reward Types' },
    { name: 'Total Response' },
    { name: 'Date Created' },
  ];
  surveyRewardForm: FormGroup;
  loading = false;
  submitted = false;
  selectSurveyForm: any;
  selectChange = false;
  save_survey: any;
  survey_id: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  save_surveyReward: any;
  redemptionpoints: any;

  
  constructor(
    private surveyrewardServices: SurveyRewardService, 
    private formBuilder: FormBuilder,
    public utility: UtilityProvider, 
    private surveyService: SurveyService,
    private authenticationService: AuthenticationService,
    private surveyQuestService: SurveyQuestService,
    private alertService: AlertService
    ) {
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user.user;
        console.log("User value", this.currentUser);
      });
      this.getAllSurveyRewards();
      this.getAllRedemptionPoints();
      setTimeout(() => { this.loadingIndicator = false; }, 1500);
  }

  getAllSurveyRewards() {
    this.authenticationService.allproductRewards.subscribe(save_surveyReward => {
      if (save_surveyReward) {
        console.log(save_surveyReward);
        this.save_surveyReward = save_surveyReward;
        this.surveyrewardServices.getAllSurveyReward(this.currentUser.id).pipe(first()).subscribe(data => {
          if (data['data'].rewards) {
            this.data = data['data'].rewards;
            this.rows = this.data;
            this.temp = [...this.data];
            localStorage.setItem('allsurveyRewards', JSON.stringify(data['data'].rewards));
          }
        });
      } else {
        this.surveyrewardServices.getAllSurveyReward(this.currentUser.id).pipe(first()).subscribe(data => {
          if (data['data'].rewards) {
            this.data = data['data'].rewards;
            // this.save_surveyReward =  this.data;
            this.rows = this.data;
            this.temp = [...this.data];
            localStorage.setItem('allsurveyRewards', JSON.stringify(data['data'].rewards));
          }
        });
      }
    });
  }
  ngOnInit() {
    this.selectSurveyForm = this.formBuilder.group({
      survey_content: ['', [Validators.required]]
    });
  }
  getAllRedemptionPoints() {
    this.surveyService.getAllRedemptionPoints(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log(data['data']);
      if (data['status']) {
        this.data = data['data'];
        this.redemptionpoints = this.data;
      }
    });
  }

}
