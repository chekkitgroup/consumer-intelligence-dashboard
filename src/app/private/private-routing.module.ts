import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { DashboardModule } from './../dashboard/dashboard.module';
import { DashboardComponent } from './../dashboard/dashboard.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { UserFeedbackComponent } from './user-feedback/user-feedback.component';
import { AllRewardsComponent } from './all-rewards/all-rewards.component';
import { RewardsComponent } from './rewards/rewards.component';
import { CodeCollectionComponent } from './code-collection/code-collection.component';
import { ViewBatchComponent } from './view-batch/view-batch.component';
import { InsightsComponent } from './insights/insights.component';
import { ProductComponent } from './product/product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { RetargetingComponent } from './retargeting/retargeting.component';
import { SurveyComponent } from './survey/survey.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { AddProductComponent } from './add-product/add-product.component';
import { CreateBatchComponent } from './create-batch/create-batch.component';
import { ProductPinsComponent } from './product-pins/product-pins.component';
import { MerchandizeRewardsComponent } from './merchandize-rewards/merchandize-rewards.component';
import { AirtimeRewardComponent } from './airtime-reward/airtime-reward.component';
import { LoyaltyPointsComponent } from './loyalty-points/loyalty-points.component';
import { AddLoyaltyPointComponent } from './add-loyalty-point/add-loyalty-point.component';
import { CreateRedemptionPointsComponent } from './create-redemption-points/create-redemption-points.component';
import { ViewLoyaltyPointComponent } from './view-loyalty-point/view-loyalty-point.component';
import { RaffleDrawComponent } from './raffle-draw/raffle-draw.component';
import { ViewRaffleComponent } from './view-raffle/view-raffle.component';
import { ProductBatchesComponent } from './product-batches/product-batches.component';
import { EditSurveyComponent } from './edit-survey/edit-survey.component';
import { ViewSurveyComponent } from './view-survey/view-survey.component'

import { ViewContactsComponent } from './view-contacts/view-contacts.component';
import { ShortcodesComponent } from './shortcodes/shortcodes.component';




export const PrivateRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'overview',
				component: DashboardComponent, 
				pathMatch: 'full',
				data: {
					title: 'Overview'
				}
			},
			{
				path: 'analytics',
				component: AnalyticsComponent,
				data: {
					title: 'Analytics',
				}
			},
			{
				path: 'products',
				component: ProductComponent,
				data: {
					title: 'Products',
				}
			},
			{
				path: 'products/product-batches',
				component: ProductBatchesComponent,
				data: {
					title: 'Product Batches',
				}
			},
			{
				path: 'products/add-product',
				component: AddProductComponent,
				data: {
					title: 'Add Product',
				}
			},
			{
				path: 'products/edit-product',
				component: EditProductComponent,
				data: {
					title: 'Edit Product',
				}
			},
			{
				path: 'insights',
				component: InsightsComponent,
				data: {
					title: 'Insights',
				}
			},
			{
				path: 'surveys',
				component: SurveyComponent,
				data: {
					title: 'Surveys',
				}
			},
			{
				path: 'surveys/create-survey',
				component: CreateSurveyComponent,
				data: {
					title: 'Create Survey',
				}
			},
			{
				path: 'surveys/edit-survey',
				component: EditSurveyComponent,
				data: {
					title: 'Edit Survey',
				}
			},
			{
				path: 'surveys/view-survey',
				component: ViewSurveyComponent,
				data: {
					title: 'View Survey',
				}
			},
			{
				path: 'rewards',
				component: RewardsComponent,
				data: {
					title: 'Rewards',
				}
			},
			{
				path: 'rewards/view-all',
				component: AllRewardsComponent,
				data: {
					title: 'Rewards',
				}
			},
			{
				path: 'rewards/merchandize-reward',
				component: MerchandizeRewardsComponent,
				data: {
					title: 'Merchandize Rewards',
				}
			},
			{
				path: 'rewards/airtime-reward',
				component: AirtimeRewardComponent,
				data: {
					title: 'Airtime Rewards',
				}
			},
			{
				path: 'rewards/loyalty-points',
				component: LoyaltyPointsComponent,
				data: {
					title: 'Loyalty Points',
				}
			},
			{
				path: 'rewards/add-loyalty-point',
				component: AddLoyaltyPointComponent,
				data: {
					title: 'Add Loyalty Point',
				}
			},
			{
				path: 'rewards/view-loyalty-point',
				component: ViewLoyaltyPointComponent,
				data: {
					title: 'View Loyalty Point',
				}
			},
			{
				path: 'rewards/raffle-draw',
				component: RaffleDrawComponent,
				data: {
					title: 'Raffle Draw',
				}
			},
			{
				path: 'rewards/view-raffle',
				component: ViewRaffleComponent,
				data: {
					title: 'Raffle Draw',
				}
			},
			{
				path: 'rewards/create-redemption-point',
				component: CreateRedemptionPointsComponent,
				data: {
					title: 'Create Redemption Point',
				}
			},
			{
				path: 'code-collections',
				component: CodeCollectionComponent,
				data: {
					title: 'Code Collections',
				}
			},
			{
				path: 'products/view-batch',
				component: ViewBatchComponent,
				data: {
					title: 'View Batch',
				}
			},
			{
				path: 'products/product-pins',
				component: ProductPinsComponent,
				data: {
					title: 'product Pins',
				}
			},
			{
				path: 'user-feedback',
				component: UserFeedbackComponent,
				data: {
					title: 'User Feedback Logs',
				}
			},
			{
				path: 'retargeting',
				component: RetargetingComponent,
				data: {
					title: 'Engage Customers',
				}
			},
			{
				path: 'products/create-batch',
				component: CreateBatchComponent,
				data: {
					title: 'Create Batch',
				}
			},
			{
				path: 'retargeting/shortcodes',
				component: ShortcodesComponent,
				data: {
					title: 'Short Codes',
				}
			},
			{
				path: 'retargeting/contacts',
				component: ViewContactsComponent,
				data: {
					title: 'View Contacts',
				}
			},
			{
				path: 'retargeting/contacts',
				component: ViewContactsComponent,
				data: {
					title: 'View Contacts',
				}
			}
			// retargeting/user-feedback
		]
		// create-batch
	},
  {
	path: '', 
	pathMatch: 'full',
    component: DashboardComponent,
    // children: [
    //   {
    //     path: 'authentication',
    //     loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardModule)
    //   }
    // ]
  },
];
