import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { first } from 'rxjs/operators';
import { Product } from '../../_models';
import { Subscription } from 'rxjs';
import { User } from '../../_models/user';
import { ProductService } from '../../_services/product.service';
import { Router } from '@angular/router';
import { UtilityProvider } from '../../_helpers/utility';

@Component({
  selector: 'app-code-collection',
  templateUrl: './code-collection.component.html',
  styleUrls: ['./code-collection.component.css']
})
export class CodeCollectionComponent implements OnInit {
  currentUserSubscription: Subscription;
  currentUser: User;
  productObjct: any;
  data: any;
  rows: any;
  temp: any[];

  constructor(
    private authenticationService: AuthenticationService, 
    public utility: UtilityProvider, 
    private productService: ProductService,
    private router: Router) {

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.get_UserAllbatches();
  }

  ngOnInit() {
  }
  get_UserAllbatches() {
    this.productService.getAllUserProductBatches(this.currentUser.id).pipe(first()).subscribe(data => {
      if (data['status']) {
        console.log(data['data'].UserproductBatches);
        this.data = data['data'].UserproductBatches;
        this.rows = this.data;
        this.temp = [...this.data];
      }
    });
  }
  gotoCreateBatch() {
    const data = this.productObjct;
    this.router.navigate(['/pages/create-batch'], { queryParams: { data }, skipLocationChange: true });
  }
  ViewBatch(product: any) {
    console.log("product", product);
    this.productObjct = product.product;

    this.productService.updateBatchDetails(product);
    this.productService.updateProductDetails(product.product);
    const slug = product?product.product?.slug:'';

    this.router.navigate(['/dashboard/products/view-batch'], { queryParams: { slug } });
  }

  gotoPrintCode(product: any) {
    this.productObjct = product.product;

    console.log(product)
    this.productService.updateBatchDetails(product);
    this.productService.updateProductDetails(product.product);
    const slug = product?product.product?.slug:'';
    
    this.router.navigate(['/dashboard/products/product-pins'], { queryParams: { slug } });
  }

}
