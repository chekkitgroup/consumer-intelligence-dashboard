import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { ProductService, AuthenticationService, SurveyService, UserService } from '../../_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-loyalty-points',
  templateUrl: './loyalty-points.component.html',
  styleUrls: ['./loyalty-points.component.css']
})
export class LoyaltyPointsComponent implements OnInit {
  editing = {};
  rows = [];
  temp = [];
  loadingIndicator = true;
  closeResult: any;
  data: any;
  surveyForm: FormGroup;
  loading = false;
  loyalt_points: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  user_recordCounts: any;

  constructor(
    private router: Router, 
    private authenticationService: AuthenticationService,
    private surveyService: SurveyService, 
    private userService: UserService
    ) {
      
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.getAllLoyaltyPoints();
    setTimeout(() => { this.loadingIndicator = false; }, 1500);
  }
  getAllLoyaltyPoints() {
    this.surveyService.getAllUserLoyaltyPoints(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log("loyalty points", data['data'].loyaltypoints);
      if (data['data'].loyaltypoints) {
        this.loyalt_points = data['data'].loyaltypoints;
      }
    });
  }
  ngOnInit() {
    this.getUserRecordCount();
  }
  getUserRecordCount() {
    this.authenticationService.recordCounts.subscribe(all_counts => {
      if (all_counts) {
        console.log('all counts', all_counts);
        this.user_recordCounts = all_counts;
        this.userService.getUserRecordCount(this.currentUser.id).pipe(first()).subscribe(async all_counts => {
          console.log('all counts', all_counts);
          if (all_counts['status']) {
            this.user_recordCounts = await all_counts['data'];
            localStorage.setItem('recordCounts', JSON.stringify(this.user_recordCounts));
          }
        });
      }
      else {
        this.userService.getUserRecordCount(this.currentUser.id).pipe(first()).subscribe(async all_counts => {
          console.log('all counts', all_counts);
          if (all_counts['status']) {
            this.user_recordCounts = await all_counts['data'];
            localStorage.setItem('recordCounts', JSON.stringify(this.user_recordCounts));
          }
        });
      }
    });
  }
  view_loyaltyPoint(loyalty_point: any) {
    this.router.navigate(['/dashboard/rewards/view-loyalty-point'], { queryParams: loyalty_point, skipLocationChange: true });
  }

}
