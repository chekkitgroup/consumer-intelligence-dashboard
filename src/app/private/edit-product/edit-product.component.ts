import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../_services/product.service';
import { AuthenticationService, SurveyService, AlertService } from '../../_services';
import { UtilityProvider } from '../../_helpers/utility';
import { first } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { SurveyRewardService } from '../../_services/surveyReward.service';


@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  productObject: any;
  productForm: FormGroup;
  loading = false;
  submitted = false;
  success_msg: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  save_surveyReward: any;
  save_survey: any;
  data: any;
  batches: any;
  slug: string;

  constructor(
    private productService: ProductService, 
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, 
    private authenticationService: AuthenticationService,
    private surveyService: SurveyService, 
    private alertService: AlertService,
    private utilityProvider: UtilityProvider,
    private surveyrewardServices: SurveyRewardService, 
    private router: Router) 
    {
      // this.productObjct = JSON.parse(this.activatedRoute.snapshot.queryParams.data);
      this.slug = this.activatedRoute.snapshot.queryParams.slug;
      this.productService.productDetails$.subscribe(
        (details) => {
          this.productObject = details;
          console.log(details)
        }
      );
  

      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user.user;
        console.log('User value', this.currentUser);
      });

      this.getProduct();
  }

  ngOnInit() {
  }

  inializeForm(){
    let eDate = new Date(this.productObject.expiry_date);
    let nDate = this.datePipe.transform(eDate, 'yyyy-MM-dd');

    let pDate = new Date(this.productObject.production_date);
    let pDateF = this.datePipe.transform(pDate, 'yyyy-MM-dd');

    console.log(nDate)

    this.productForm = this.formBuilder.group({
      product_name: [this.productObject.product_name, Validators.required],
      product_description: [this.productObject.product_description, Validators.required],
      batch_num: [this.productObject.batch_num, Validators.required],
      expiry_date: [nDate, Validators.required],
      id_range: [this.productObject.id_range, Validators.required],
      product_category: [this.productObject.product_category, Validators.required],
      barcode_num: [this.productObject.barcode_num, Validators.required],
      production_date: [pDateF, Validators.required],
      // surveyId: [this.productObject.surveyId, Validators.required],
      // rewardId: [this.productObjct.rewardId, Validators.required],
      age_range: [this.productObject.age_range, Validators.required],
    });
    console.log(this.productObject)
  }

  getProduct() {
    this.productService.getProduct(this.slug).pipe(first()).subscribe(data => {
      console.log("result of survey", data['data']);
      if (data['data']) {
        console.log('okay ooo', data['data']);
        this.productObject = data['data'];
        this.inializeForm();
        this.getAllSurveys();
        this.getAllSurveyRewards();
        this.get_Allbatches();
      }
    });
  }

  get f() { return this.productForm.controls; }
  getAllSurveys() {
    this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log("result of survey", data['data']);
      if (data['data'].surveys) {
        console.log('okay ooo', data['data'].surveys);
        this.data = data['data'].surveys;
        this.save_survey = this.data;
        localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
      }
    });
  }
  getAllSurveyRewards() {
    this.surveyrewardServices.getAllSurveyReward(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log("result of reward", data['data']);
      if (data['data'].rewards) {
        this.data = data['data'].rewards;
        this.save_surveyReward = this.data;
        console.log("rewards", this.save_surveyReward);
        localStorage.setItem('allsurveyRewards', JSON.stringify(data['data'].rewards));
      }
    });
  }
  onSubmit() {
    console.log('get here');
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.productForm.invalid);
    if (this.productForm.invalid) {
      return;
    }
    this.loading = true;
    console.log(this.productForm.value);
    const formData = new FormData();
    formData.append('product_description', this.productForm.get('product_description').value);
    formData.append('product_name', this.productForm.get('product_name').value);
    formData.append('batch_num', this.productForm.get('batch_num').value);
    formData.append('barcode_num', this.productForm.get('barcode_num').value);
    formData.append('id_range', this.productForm.get('id_range').value);
    formData.append('age_range', this.productForm.get('age_range').value);
    formData.append('expiry_date', this.productForm.get('expiry_date').value);
    formData.append('production_date', this.productForm.get('production_date').value);
    formData.append('product_category', this.productForm.get('product_category').value);
    this.productService.update_product(this.productObject.slug, this.productForm.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('ok>>', data['data']);
          if (data['status']) {
            // this.success_msg = 'Product updated successfully.';
            alert('Product updated successfully.');
            this.utilityProvider.showToast('success',this.success_msg );

            console.log('oka 3 >>', data['data']);
            // this.router.navigate(['/pages/product']);
            // this.updateSurveyProduct();
            this.loading = false;

          } else {
            this.success_msg = data['message'];
            this.loading = false;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });

        // this.updateSurveyProduct();
  }
  updateProduct() {
    this.onSubmit();
  }
  get_Allbatches() {
    this.productService.getAllProductBatches(this.productObject.id).pipe(first()).subscribe(data => {
      if (data['status']) {
        console.log(data['data'].productBatches);
        this.batches = data['data'].productBatches;
      }
    });
  }

  updateSurveyProduct() {
    this.surveyService.updateProductSurvey2(this.productForm.get('surveyId').value, { productId: this.productObject.id })
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          console.log('ok>>', data);
          if (data['body'].status) {
            this.success_msg = data['message'];
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

}
