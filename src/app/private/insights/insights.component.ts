import { Component, HostBinding, ViewChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService, SurveyService,ProductService, RestService } from '../../_services';
import { first } from 'rxjs/operators';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { UserService } from '../../_services/user.service';
import { AddRewardComponent } from '../../shared/modals/add-reward/add-reward.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExportToCsv } from 'export-to-csv';
import { fromEvent } from 'rxjs';
import { map, debounceTime } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import * as c3 from 'c3';
import { UtilityProvider } from '../../_helpers';



import * as moment from 'moment';
declare var google;


interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'app-insights',
  templateUrl: './insights.component.html',
  styleUrls: ['./insights.component.css']
})
export class InsightsComponent implements OnInit {

  private index: number = 0;

  @HostBinding('class')
  classes = 'example-items-rows';
  showSurvey = false;
  title = 'dashboard';
  chart;
  chart2 = [];
  pie: any;
  doughnut: any;
  callData: any;
  giftData: any;
  rows: any[];
  data: any;
  temp: any[];
  columns: any[];
  scanStatistics;
  graphData: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  @ViewChild('map', { static: true }) mapElement: ElementRef;
  map: any;
  ageDistribution: any;
  genderDistribution: any;
  rows2: any;
  rows3: any;
  row;
  winnersList = [];
  submitting;
  myDate = new Date();
  aWeekBefore = new Date(this.myDate.getTime() - 60 * 60 * 24 * 7 * 1000);
  limit= 25;
  setupStage = 2;
  setupmode = '';

  public filterOptions = {
    startDate : moment(this.aWeekBefore).format('YYYY-MM-DD'),
    endDate : moment(this.myDate).format('YYYY-MM-DD'),
    type : "dials",
  };

 

  @ViewChild('search', { static: false }) search: any;
  setupChanelType: any;
  loading: boolean;
  surveys: any;
  loadingCode: boolean;
	inputData = {
		short_code: "",
		survey_id: "",
	};



  constructor(
    private productService: ProductService, 
    private modalService: NgbModal,
    private formBuilder: FormBuilder, 
    public utility:UtilityProvider,
    private router: Router,
    private _rest: RestService, 
    private authenticationService: AuthenticationService, 
    private surveyService: SurveyService,
    private httpClient: HttpClient,
    private userService: UserService) {
      
      

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });

  }
  ngOnInit() {

    this.getVoiceCallLeads();
    this.getGifts();
    this.getDistribution();
    this.getWinners();
    this.getSignUps();
    this.getDailyDials();
    this.getAllSurvey();
    this.get3DigitCode();
   
     this.columns = [
      { prop: 'id' }, 
      { prop: 'name' }, 
      { prop: 'email' }, 
      { prop: 'phone'},
      { prop: 'age'},
      { prop: 'gender'},
      { prop: 'uv_id'},
      { prop: 'winning_points'},
      { prop: 'category'},
      { prop: 'gift_type'},
      // { prop: 'age', name: 'Age' }
    ];

    // this.getDataJson();  
  }

  // ngOnDestroy() {
  //   this.alive = false;
  // }

  showToast(position, status) {
    // this.index += 1;
    // this.toastrService.show(
    //   status || 'Success',
    //   // `Status ${this.index}`,
    //   'Status Updated',
    //   { position, status });
  }

  get3DigitCode() {
    this.loadingCode = true;
    this._rest.get3DigitCode().pipe(first()).subscribe(data => {
      if (data['status']) {
        // this.plan = data['data'].plan;
        this.inputData.short_code = data['data'].short_code;
        this.loadingCode = false;
        // console.log(this.inputData.short_code)
      }
    },
    error => {
      console.log(error)
      this.loadingCode = false;
    });
  }
  ngAfterViewInit(): void {
    // Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    // Add 'implements AfterViewInit' to the class.
    // fromEvent(this.search.nativeElement, 'keydown')
    //   .pipe(
    //     debounceTime(550),
    //     map(x => x['target']['value'])
    //   )
    //   .subscribe(value => {
    //     this.updateFilter(value);
    //   });
  }

  updateWinnerStatus(id,status){
    console.log(status)
    let data = { id,status}

    this._rest.addStatus(data).pipe(first()).subscribe(data => {
      // this.scanStatistics = data['data'];
      console.log(data['data']);
      this.showToast('top-right', 'success')
    });;
  }

  updateFilter(val: any) {
    const value = val.toString().toLowerCase().trim();
    // get the amount of columns in the table
    const count = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.temp[0]);
    // assign filtered matches to the active datatable
    this.rows = this.temp.filter(item => {
      // iterate through each row's column data
      for (let i = 0; i < count; i++) {
        // console.log(keys[i])
        // check for a match
        if (
          (item[keys[i]] &&
            item[keys[i]]
              .toString()
              .toLowerCase()
              .indexOf(value) !== -1) ||
          !value
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
  }



  openAddRewardModal() {
    const modalRef = this.modalService.open(AddRewardComponent, { size: 'lg',centered: true  });
    // modalRef.componentInstance.inputData = row;
    modalRef.result.then((result) => {
      console.log(result)
      if(result){
        this.getWinners() 
      }
      // this._success.next("Successfully Deleted");
    }).catch((res) => {
      console.log(res)
      if(res){
        this.getWinners()
      }
    });
  }
  selectChannel(t){
    this.setupStage = 3;
    this.setupChanelType = t;
  }
  surveyAdded(){

    // this.success_msg = "Survey added successfully.";
    this.router.navigate(['/dashboard/survey']);

    // if(this.currentUser.access_level == 2){
    //   this.router.navigate(['/pages/survey']);
    //   this.generateSurveyUssdModal(data['data'].id);
    // }else{
    //   this.router.navigate(['/pages/survey']);
    // }
  }

  getSignUps(){
    this._rest.getSignUps().pipe(first()).subscribe(data => {
      // this.scanStatistics = data['data'];
      console.log(data['data']);
      this.rows3 = data['data'];
    });
  } 


 getVoiceCallLeads() {
  this._rest.getVoiceCallLeads().pipe(first()).subscribe(data => {
    // this.scanStatistics = data['data'];
    console.log(data['data']);
    this.callData = data['data'];
  });
}

getGifts() {
  this._rest.getGifts().pipe(first()).subscribe(data => {
    // this.scanStatistics = data['data'];
    console.log(data['data']);
    this.rows = data['data'];

    this.rows.forEach(item => {
        this.winnersList[item.id] = item.status
    });
  });
}

getWinners() {
  this._rest.getWinners().pipe(first()).subscribe(data => {
    // this.scanStatistics = data['data'];
    console.log(data['data']);
    this.rows2 = data['data'];
  });
}


getAllSurvey() {
  // this.loading = true;
  this.authenticationService.allsurveys.subscribe(save_survey => {
    console.log(save_survey);
    if (save_survey) {
      this.surveys = save_survey;
      this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
        if (data['data'].surveys) {
          this.surveys = data['data'].surveys;
          this.loading = false;
        }
      });
    } else {
      this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
        console.log(data['data'].surveys);
        if (data['data'].surveys) {
          this.surveys = data['data'].surveys;
          // this.loading = false;
        }
      });
    }
  });
}
getDailyDials() {
  this._rest.getDailyDials(this.filterOptions.type,this.filterOptions.startDate,this.filterOptions.endDate).pipe(first()).subscribe(data => {
    // this.scanStatistics = data['data'];
    console.log(data['data']);
    this.graphData = data['data'];

    let values = [];
    let labels = [];


    this.graphData.forEach(obj => {
      // labels.push(moment(obj.created_at).format('YYYY-MM-DD'))
      labels.push(moment(obj.created_at).format('MMM DD'))
      values.push(obj.count)
    });

    labels.unshift('')
    values.unshift(0)

    this.graphData.graphLabels = labels;
    this.graphData.graphValues = values;
    console.log(values)
  });
}





generateExcel(action = 'open') {
  var mappedPins = this.rows.map(p => ({ name: p.name, phone: p.phone, gift_type: p.gift_type, winning_points: p.winning_points, category: p.category }));

  console.log(mappedPins);

  var d = new Date();
  var n = d.getTime();
  let newFileName = 'chekkit-List of Winners-' + n;

  const options = { 
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalSeparator: '.',
    showLabels: true, 
    showTitle: true,
    title: 'Chekkit List of Winners CSV',
    useTextFile: false,
    useBom: true,
    filename: newFileName,
    useKeysAsHeaders: false,
    headers: ['Name', 'Phone Number', 'Gift Type', 'Winning Point', 'Category']
  };
    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(mappedPins);    
}



getDistribution() {
  this._rest.getDistribution().pipe(first()).subscribe(data => {
    this.ageDistribution = data['data'].age;
    this.genderDistribution = data['data'].gender;

    this.ageDistribution.labels = [];
    this.ageDistribution.values = [];

    this.genderDistribution.labels = [];
    this.genderDistribution.values = [];

    for (let i = 0; i < this.ageDistribution.length; i++) {
      if(this.ageDistribution[i].age && this.ageDistribution[i].ageCount){
        this.ageDistribution.values.push(this.ageDistribution[i].ageCount)
        this.ageDistribution.labels.push(this.ageDistribution[i].age)
      }

      // console.log(this.ageDistribution[i]);
    }
    
    for (let i = 0; i < this.genderDistribution.length; i++) {
      if(this.genderDistribution[i].gender && this.genderDistribution[i].genderCount){
        this.genderDistribution.labels.push(this.genderDistribution[i].gender)      
        this.genderDistribution.values.push(this.genderDistribution[i].genderCount)
      }
    }
    // console.log(this.genderDistribution);
  
  });
}

}
