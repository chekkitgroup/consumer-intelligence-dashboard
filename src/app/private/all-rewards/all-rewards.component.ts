import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { first } from 'rxjs/operators';
import { Product } from '../../_models';
import { Subscription } from 'rxjs';
import { User } from '../../_models/user';
import { ProductService } from '../../_services/product.service';
import { Router } from '@angular/router';
import { SurveyRewardService } from '../../_services/surveyReward.service';

@Component({
  selector: 'app-all-rewards',
  templateUrl: './all-rewards.component.html',
  styleUrls: ['./all-rewards.component.css']
})
export class AllRewardsComponent implements OnInit {
  currentUserSubscription: Subscription;
  currentUser: User;
  rewardObjct: any;
  data: any;
  rows: any;
  temp: any[];


  
  constructor(
    private authenticationService: AuthenticationService, 
    private productService: ProductService,
    private router: Router, 
    private _surveyRewardService: SurveyRewardService
    ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.get_UserRewards();
  }

  ngOnInit(): void {
  }
  get_UserRewards() {
    this._surveyRewardService.getAllUserSurveyReward(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log("data", data);
      if (data['status']) {
        console.log(data['data']);
        this.data = data['data'];
        this.rows = this.data;
        this.temp = [...this.data];
      }
    });
  }
  gotoCreateBatch() {
    this.router.navigate(['/pages/create-batch'], { queryParams: this.rewardObjct, skipLocationChange: true });
  }

  ViewReward(reward: any) {
      console.log("product", reward);
      this.rewardObjct = reward;
      const data = JSON.stringify({
        reward: reward,
      });
      if (reward.reward_type == 'Airtime') {
        this.router.navigate(['/pages/edit-airtime'], { queryParams: { data }, skipLocationChange: true });
      } else {
        this.router.navigate(['/pages/edit-merchandize'], { queryParams: { data }, skipLocationChange: true });
      }

  }
  
  EditReward(product: any) {
    this.rewardObjct = product.product;
    const data = JSON.stringify({
      batch: product,
      product: product.product
    });
    this.router.navigate(['/pages/edit-reward'], { queryParams: { data }, skipLocationChange: true });
  }

}
