import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService, SurveyService, SurveyRewardService, AlertService, ProductService } from '../../_services';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { User } from '../../_models';
import { UserService } from '../../_services/user.service';
import * as moment from 'moment';
declare var google;


@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {
  editing = {};
  rows = [];
  temp = [];
  success_msg: any;
  loadingIndicator = true;
  reorderable = true;
  closeResult: any;
  data: any;
  surveyForm: FormGroup;
  loading = false;
  submitted = false;
  save_survey: any;
  save_products: any;
  selectChange: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  user_recordCounts: any;
  products: any;
  myDate = new Date();
  aWeekBefore = new Date(this.myDate.getTime() - 60 * 60 * 24 * 7 * 1000);
  aMonthTime = new Date(this.myDate.getTime() + 60 * 60 * 24 * 30 * 1000);

  @ViewChild('map', { static: true }) mapElement: ElementRef;
  map: any;


  // lineChart
  public lineChartData: Array<any> = [
  ];
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: 'rgba(54,190,166,.1)',
      borderColor: '#36bea6',
      pointBackgroundColor: '#36bea6',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#36bea6'
    },
    {
      // dark grey
      backgroundColor: 'rgb(41,98,255,.1)',
      borderColor: '#2962FF',
      pointBackgroundColor: '#2962FF',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#2962FF'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  scanStatistics: any;
  scanCords :any = [];
  heatmap: any;


  public filterOptions = {
    startDate : moment(this.aWeekBefore).format('YYYY-MM-DD'),
    endDate : moment(this.myDate).format('YYYY-MM-DD'),
    productId : "",
  };


 
  constructor(
    private productService: ProductService, 
    private formBuilder: FormBuilder, 
    private router: Router,
    private authenticationService: AuthenticationService, 
    private surveyService: SurveyService,
    private userService: UserService
    ) {

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.getAllSurvey();
    this.getAllProducts();
    setTimeout(() => { this.loadingIndicator = false; }, 1500);
  }
  ngOnInit() {
    this.initMap()
  }


  initMap(){
    this.getPosition().then(pos=>
      {
        //  console.log(`Positon: ${pos.lng} ${pos.lat}`);
        var mapProp = {
          center: new google.maps.LatLng(pos.lat,pos.lng),
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true,
          zoomControl: true,
          scaleControl: false,
        };
        // this.map = new google.maps.Map(this.map1, mapProp);
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProp);
      });
  }

  getAllSurvey() {
    this.authenticationService.allsurveys.subscribe(save_survey => {
      console.log(save_survey);
      console.log('printed Surveys up');
      if (save_survey) {
        this.save_survey = save_survey;
        this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
          if (data['data'].surveys) {
            this.save_survey = data['data'].surveys;
            // localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
          }
        });
      } else {
        this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
          console.log(data['data'].surveys);
          if (data['data'].surveys) {
            this.save_survey = data['data'].surveys;
            localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
          }
        });
      }
    });
  }
  getAllProducts() {
    this.authenticationService.allProducts.subscribe(save_products => {
      if (save_products) {
        this.save_products = save_products;
        this.productService.getAllUserProducts(this.currentUser.id).pipe(first()).subscribe(data => {
          console.log('to save ', data['data'].products);
          if (data['data'].products) {
            this.products = data['data'].products;
            let product = this.products[0];
            this.filterOptions.productId = product.id;
            this.getProductScansByDate()
            this.getProductScans()
          }
        });
      } else {
        this.productService.getAllUserProducts(this.currentUser.id).pipe(first()).subscribe(data => {
          console.log(data['data'].products);
          if (data['data'].products) {
            this.products = data['data'].products;
            let product = this.products[0];
            this.filterOptions.productId = product.id;
            this.getProductScansByDate()
            this.getProductScans()
          }
        });
      }
    });
  }

  getProductScansByDate() {
    // console.log(productId)
    this.scanCords = [];
    this.productService.getScansByDate(this.filterOptions.productId,this.filterOptions.startDate,this.filterOptions.endDate).pipe(first()).subscribe(data => {
      this.scanStatistics = data['data'];
      console.log(data['data']);
      let vals = data['data'];
      let dailyScans = vals.map(function (v) {
        return v.count
      });
      let scanDates = vals.map(function (v) {
        return v.date
      });
      dailyScans.unshift(0);

      this.getProductScans()
      this.lineChartData = [{ data: dailyScans, label: 'Scans per day' }];
      this.lineChartLabels = scanDates;
      console.log(dailyScans)
      // if (data['data'].surveys) {
      //   this.save_survey = data['data'].surveys;
      // }
    });
  }

  async geoCode(address:any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      if(status == 'OK'){
        let cords = {latitude:results[0].geometry.location.lat(), longitude: results[0].geometry.location.lng()}
        let latLng = new google.maps.LatLng(cords.latitude, cords.longitude);

        this.scanCords.push(latLng);
        
        this.heatmap = new google.maps.visualization.HeatmapLayer({
          data: this.scanCords,
          map: this.map
        });
        return cords;
       
      }
   });
 }

  getProductScans() {
    console.log('productId')
    this.productService.getProductScans(this.filterOptions.productId).pipe(first()).subscribe(data => {
      this.scanStatistics = data['data'];
      // console.log(data['data']);
      console.log(this.scanStatistics.length);

      if(this.scanStatistics.length < 1){
        // let heatmap = new google.maps.visualization.HeatmapLayer({
        //   data: [],
        //   map: this.map
        // });
        this.initMap()
        // this.heatmap.setMap(null);
      }else{
        this.convertLocations(this.scanStatistics);
      }
    });
  }


  convertLocations(locations){

    // this.map.clearOverlays();

    var n:number = locations.length;
    do { 
      //  console.log(locations[n].scan_location); 
      if(locations[n] && locations[n].scan_location){
        this.geoCode(locations[n].scan_location)
      }
      //  this.geoCode(locations[n].scan_location);
       n--; 
       if(n == 0){
        //  console.log(this.scanCords)
       }
    } while(n>=0);
  } 

  getPosition(): Promise<any>{
    return new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition(resp => {

          resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
        },
        err => {
          reject(err);
        });
    });

  }
}
