import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ProductService } from '../../_services/product.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { AlertService } from '../../_services/alert.service';
import { User } from '../../_models/user';
import { SurveyService } from '../../_services/survey.service';
import { SurveyRewardService } from '../../_services';

@Component({
  selector: 'app-merchandize-rewards',
  templateUrl: './merchandize-rewards.component.html',
  styleUrls: ['./merchandize-rewards.component.css']
})
export class MerchandizeRewardsComponent implements OnInit {


  imageUrl: any;
  success_msg: any;
  surveyMerchantForm: FormGroup;
  loading = false;
  save_products: any;
  save_surveyReward: any;
  save_survey: any;
  selectChange: any;
  currentUser: User;
  submitted: any;
  currentUserSubscription: Subscription;

  constructor(
    private productService: ProductService, 
    private formBuilder: FormBuilder, 
    private router: Router,
    private authenticationService: AuthenticationService, 
    private surveyService: SurveyService,
    private alertService: AlertService, 
    private surveyrewardServices: SurveyRewardService
    ) {

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
  }
  ngOnInit() {
    this.surveyMerchantForm = this.formBuilder.group({
      reward_value: ['', Validators.required],
      reward_quant: ['', Validators.required],
      reward_type: ['Merchandize', Validators.required],
      reward_point: ['', Validators.required],
      photo: ['', Validators.required],
    });
  }
  /* Select Dropdown error handling */
  public handleError = (controlName: string, errorName: string) => {
    console.log('get here ');
    console.log(this.surveyMerchantForm.controls[controlName].hasError(errorName));
    if (this.surveyMerchantForm.controls[controlName].hasError(errorName)) {
      this.selectChange = false;
    }
    else {
      this.selectChange = true;
    }
    return this.surveyMerchantForm.controls[controlName].hasError(errorName);
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.surveyMerchantForm.get('photo').setValue(file);
    }
  }
  get f() { return this.surveyMerchantForm.controls; }

  onSubmit() {
    console.log('get here');
    // stop here if form is invalid
    console.log(this.surveyMerchantForm.invalid);
    if (this.surveyMerchantForm.invalid) {
      return;
    }
    this.loading = true;
    console.log(this.surveyMerchantForm.value);
    const formData = new FormData();
    formData.append('photo', this.surveyMerchantForm.get('photo').value);
    formData.append('reward_value', this.surveyMerchantForm.get('reward_value').value);
    formData.append('reward_type', this.surveyMerchantForm.get('reward_type').value);
    formData.append('reward_quant', this.surveyMerchantForm.get('reward_quant').value);
    formData.append('reward_point', this.surveyMerchantForm.get('reward_point').value);

    this.surveyrewardServices.createMerchatReward(this.currentUser.id, formData)
      .pipe(first())
      .subscribe(
        data => {
          console.log('ok>>', data['data']);
          if (data['status']) {
            console.log('oka 3 >>', data['data']);
            // this.data = this.save_products;
            this.router.navigate(['/dashboard/product']);
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

}
