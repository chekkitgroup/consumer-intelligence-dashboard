import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ProductService, AuthenticationService } from '../../_services';
import { User } from '../../_models/user';
import { UtilityProvider } from '../../_helpers/utility';

@Component({
  selector: 'app-view-batch',
  templateUrl: './view-batch.component.html',
  styleUrls: ['./view-batch.component.css']
})
export class ViewBatchComponent implements OnInit {

  batchObject: any;
  productObject: any;
  navData: any;
  currentUserSubscription: Subscription;
  currentUser: User;
  
  constructor(
    private productService: ProductService,
    public utility: UtilityProvider, 
    private activatedRoute: ActivatedRoute, 
    private authenticationService: AuthenticationService,
    private router: Router
    ) {
    this.navData = this.activatedRoute.snapshot.queryParams;
    console.log('product', (this.navData));

    this.productService.productDetails$.subscribe(
      (details) => {
        this.productObject = details;
        console.log(details)
      }
    );

    this.productService.batchDetails$.subscribe(
      (details) => {
        this.batchObject = details;
        console.log(details)
      }
    );

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
  }
  ngOnInit(): void {
  }

  getShelfLife(prod_date: any, exp_date) {
    const diff = (new Date(exp_date)).valueOf() - (new Date(prod_date)).valueOf();
    console.log('result date ', Math.ceil(diff / (1000 * 3600 * 24)));
    return Math.ceil(diff / (1000 * 3600 * 24));
  }

  gotoBatchPins() {
    this.productService.updateBatchDetails(this.batchObject);
    this.productService.updateProductDetails(this.productObject);
    const slug = this.productObject?this.productObject.product?.slug:'';
    
    this.router.navigate(['/dashboard/products/product-pins'], { queryParams: { slug } });
  }

  gotoCreateBatch() {
    let q = JSON.stringify(this.batchObject.id);
    this.router.navigate(['/dashboard/products/create-batch'], { queryParams: { q }});
  }

}
