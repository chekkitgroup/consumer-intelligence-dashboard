import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
import { SurveyQuestService } from '../../_services/surveyQuest.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../../_services/alert.service';
import { ToastService } from '../../_services/toast.service';
import { SurveyRewardService } from '../../_services/surveyReward.service';
import { User } from '../../_models/user';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../_services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-redemption-points',
  templateUrl: './create-redemption-points.component.html',
  styleUrls: ['./create-redemption-points.component.css']
})
export class CreateRedemptionPointsComponent implements OnInit {
  redemptionPointForm: FormGroup;
  loading = false;
  submitted = false;
  currentUser: User;
  data: any;
  save_surveyReward: any;
  currentUserSubscription: Subscription;
  names: any;
  locations: any;


  constructor(
    private formBuilder: FormBuilder, 
    private surveyQuestService: SurveyQuestService,
    private alertService: AlertService, 
    private toastService: ToastService, 
    private router: Router,
    // private dialogService: NbDialogService,
    private surveyrewardServices: SurveyRewardService, 
    private authenticationService: AuthenticationService
    ) {
      
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.getAllSurveyRewards();
    this.getUserRedemptionLocations();
  }
  ngOnInit() {
    this.redemptionPointForm = this.formBuilder.group({
      point_address: ['', Validators.required],
      point_agent_code: ['', Validators.required],
      point_agent_name: ['', Validators.required],
      point_agent_phone: ['', Validators.required],
      point_location: ['', Validators.required],
      survey_rewardId: ['', Validators.required],
    });
  }

  openLocationModal() {
    // this.dialogService.open(AddLocationModal)
    //   .onClose.subscribe(name => name && this.names.push(name));
  }
  
  getAllSurveyRewards() {
    this.surveyrewardServices.getAllSurveyReward(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log("result of reward", data['data']);
      if (data['data'].rewards) {
        this.data = data['data'].rewards;
        this.save_surveyReward = this.data;
        console.log("rewards", this.save_surveyReward);
      }
    });
  }

  
  getUserRedemptionLocations() {
    this.surveyrewardServices.getUserRedemptionLocations(this.currentUser.id).pipe(first()).subscribe(data => {
      if (data['data']) {
        this.locations = data['data'];
      }
    });
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    const formData = this.redemptionPointForm.value;
    console.log(this.redemptionPointForm.value);
    this.surveyQuestService.CreateRedemptionPoint(formData)
      .pipe(first())
      .subscribe(
        data => {
          console.log("ok>>", data['data']);
          if (data['status']) {
            this.router.navigate(['/dashboard/rewards']);
          }
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}



// @Component({
//   selector: 'app-some-diaglog',
//   template: `
//       <h3>Add Location</h3>
//       <form class="form-horizontal" role="form" [formGroup]="addLocationForm" (ngSubmit)="onSubmit()">

//           <div class="form-group">

//               <label class="col-md-12 col-form-label">Agent Name</label>

//               <div class="col-md-12">

//                   <input type="text" formControlName="name" nbInput fullWidth placeholder="Location name">

//               </div>

//           </div>
//           <br>

//           <div class="text-center">
//               <button type="submit" class="btn btn-custom btn-rounded" [disabled]="loading || addLocationForm.invalid">Add Location</button>
//           </div>
//       </form>

//   `,
// })
// export class AddLocationModal {
//   addLocationForm: FormGroup;
//   loading = false;
//   submitted = false;
//   currentUser: any;

//   constructor(
//     private formBuilder: FormBuilder,
//     private alertService: AlertService, 
//     private toastService: ToastService, 
//     // private dialogService: NbDialogService,
//     private authenticationService: AuthenticationService,
//     private surveyrewardServices: SurveyRewardService, 
//     ) {
//       this.authenticationService.currentUser.subscribe(user => {
//         this.currentUser = user.data.user;
//       });
//   }
//   ngOnInit() {
//     this.addLocationForm = this.formBuilder.group({
//       name: ['', Validators.required],
//       id: [this.currentUser.id],
//     });
//   }
//   onSubmit() {
//     this.submitted = true;
//     this.loading = true;
//     const formData = this.addLocationForm.value;
//     console.log(this.addLocationForm.value);
//     this.surveyrewardServices.addLocation(formData)
//       .pipe(first())
//       .subscribe(
//         data => {
//           console.log("ok>>", data['data']);
//           if (data['status']) {
//             // this.toastService.showToast('success','success','location added');
//             this.addLocationForm.setValue({
//               name: '',
//               id: this.currentUser.id,
//             });
//             // this.router.navigate(['/pages/rewards']);
//           }
//           this.loading = false;
//         },
//         error => {
//           this.alertService.error(error);
//           this.loading = false;
//         });
//   }
// }