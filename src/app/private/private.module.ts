import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyticsComponent } from './analytics/analytics.component';
import { UserFeedbackComponent } from './user-feedback/user-feedback.component';
import { AddLoyaltyPointComponent } from './add-loyalty-point/add-loyalty-point.component';
import { AddProductComponent } from './add-product/add-product.component';
import { AirtimeRewardComponent } from './airtime-reward/airtime-reward.component';
import { AllRewardsComponent } from './all-rewards/all-rewards.component';
import { CodeCollectionComponent } from './code-collection/code-collection.component';
import { CreateBatchComponent } from './create-batch/create-batch.component';
import { CreateRedemptionPointsComponent } from './create-redemption-points/create-redemption-points.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { EditAirtimeComponent } from './edit-airtime/edit-airtime.component';
import { EditBatchComponent } from './edit-batch/edit-batch.component';
import { EditMerchandizeComponent } from './edit-merchandize/edit-merchandize.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { EditSurveyComponent } from './edit-survey/edit-survey.component';
import { GeneratePinComponent } from './generate-pin/generate-pin.component';
import { InsightsComponent } from './insights/insights.component';
import { LoyaltyPointsComponent } from './loyalty-points/loyalty-points.component';
import { MerchandizeRewardsComponent } from './merchandize-rewards/merchandize-rewards.component';
import { NewPinComponent } from './new-pin/new-pin.component';
import { PaymentsComponent } from './payments/payments.component';
import { ProductComponent } from './product/product.component';
import { ProductBatchesComponent } from './product-batches/product-batches.component';
import { ProductPinsComponent } from './product-pins/product-pins.component';
import { ProfileComponent } from './profile/profile.component';
import { RaffleDrawComponent } from './raffle-draw/raffle-draw.component';
import { RetargetingComponent } from './retargeting/retargeting.component';
import { RewardsComponent } from './rewards/rewards.component';
import { SurveyComponent } from './survey/survey.component';
import { ViewBatchComponent } from './view-batch/view-batch.component';
import { ViewContactsComponent } from './view-contacts/view-contacts.component';
import { ViewLoyaltyPointComponent } from './view-loyalty-point/view-loyalty-point.component';
import { ViewRaffleComponent } from './view-raffle/view-raffle.component';
import { ViewRewardComponent } from './view-reward/view-reward.component';
import { ViewSurveyComponent } from './view-survey/view-survey.component';
import { PrivateRoutes } from './private-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MomentModule } from 'ngx-moment';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';

import { ChartModule } from 'angular2-chartjs';
import { ChartsModule } from 'ng2-charts';

import { NgbModule,NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { ShortcodesComponent } from './shortcodes/shortcodes.component';
import { SetupRewardsComponent } from './setup-rewards/setup-rewards.component';
import { AddSurveyComponent } from '../shared/features/add-survey/add-survey.component';

const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};

@NgModule({
  declarations: [AddSurveyComponent, AnalyticsComponent, UserFeedbackComponent, AddLoyaltyPointComponent, AddProductComponent, AirtimeRewardComponent, AllRewardsComponent, CodeCollectionComponent, CreateBatchComponent, CreateRedemptionPointsComponent, CreateSurveyComponent, EditAirtimeComponent, EditBatchComponent, EditMerchandizeComponent, EditProductComponent, EditSurveyComponent, GeneratePinComponent, InsightsComponent, LoyaltyPointsComponent, MerchandizeRewardsComponent, NewPinComponent, PaymentsComponent, ProductComponent, ProductBatchesComponent, ProductPinsComponent, ProfileComponent, RaffleDrawComponent, RetargetingComponent, RewardsComponent, SurveyComponent, ViewBatchComponent, ViewContactsComponent, ViewLoyaltyPointComponent, ViewRaffleComponent, ViewRewardComponent, ViewSurveyComponent, ShortcodesComponent, SetupRewardsComponent],
  imports: [
    CommonModule,
    FormsModule,
    MomentModule,
    ChartModule,
    ChartsModule,
    NgxDatatableModule,
    NgbNavModule,
    ReactiveFormsModule,
    NgWizardModule.forRoot(ngWizardConfig),
    RouterModule.forChild(PrivateRoutes),
  ]
})
export class PrivateModule { }
