import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../../_models/user';
import { AuthenticationService, ProductService } from '../../_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-user-feedback',
  templateUrl: './user-feedback.component.html',
  styleUrls: ['./user-feedback.component.css']
})
export class UserFeedbackComponent implements OnInit {
  currentUserSubscription: Subscription;
  currentUser: User;
  productObjct: any;
  data: any;
  rows: any;
  temp: any[];

  constructor(
    private authenticationService: AuthenticationService, 
    private productService: ProductService,
    private router: Router
    ) {
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user.user;
        console.log('User value', this.currentUser);
      });
      this.get_UserAllbatches();
  }

  ngOnInit() {
  }
  
  get_UserAllbatches() {
    this.productService.getAllUserProductReports(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log("data", data);
      if (data['status']) {
        console.log(data['data'].reports);
        this.data = data['data'].reports;
        this.rows = this.data;
        this.temp = [...this.data];
      }
    });
  }

}
