import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SurveyService, AlertService } from '../../_services';
import { Router } from '@angular/router';
import { noWhitespaceValidator } from '../../_helpers/whitespace-validator';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-add-loyalty-point',
  templateUrl: './add-loyalty-point.component.html',
  styleUrls: ['./add-loyalty-point.component.css']
})
export class AddLoyaltyPointComponent implements OnInit {
  loyaltyPointForm: FormGroup;
  loading = false;
  submitted = false;
  success_msg: any;

  constructor(
    private formBuilder: FormBuilder, 
    private surveyService: SurveyService,
    private alertService: AlertService, 
    private router: Router, 
    private authenticationService: AuthenticationService) {
    // if (!authenticationService.tokenExist()) {
    //   router.navigate(['pages/auth/login']);
    // }
  }

  ngOnInit() {
    this.loyaltyPointForm = this.formBuilder.group({
      loyalty_name: ['', Validators.required],
      scan_point_to_allocate: ['', Validators.required],
      loyalty_reward: ['', Validators.required],
      loyalty_reward_value: ['', Validators.required],
      loyalty_point_convert: ['', Validators.required],
      // userId: ['', Validators.required],
    });
  }
  
  onSubmit() {
    this.submitted = true;
    this.loading = true;
    console.log(this.loyaltyPointForm.value);
    this.surveyService.addLoyaltyPoint(this.loyaltyPointForm.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log("ok>>", data['data']);
          if (data['status']) {
            this.router.navigate(['/pages/loyalty-points']);
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
