import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { ProductService } from '../../_services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SurveyRewardService } from '../../_services/surveyReward.service';
import { SurveyService } from '../../_services/survey.service';
import { AuthenticationService } from '../../_services/authentication.service';
import * as moment from 'moment';
import { CurrencyPipe, DatePipe, DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-edit-batch',
  templateUrl: './edit-batch.component.html',
  styleUrls: ['./edit-batch.component.css']
})
export class EditBatchComponent implements OnInit {
  success_msg: any;
  loadingIndicator = true;
  closeResult: any;
  data: any;
  subProductForm: FormGroup;
  loading = false;
  submitted = false;
  productObject: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  rows: any;
  temp: any[];
  alertService: any;
  save_survey: any;
  save_surveyReward: any;

  constructor(
    private productService: ProductService, 
    private activatedRoute: ActivatedRoute, 
    private surveyService: SurveyService,
    private formBuilder: FormBuilder, 
    private router: Router, 
    private surveyrewardServices: SurveyRewardService,
    private authenticationService: AuthenticationService, 
    private _date: DatePipe
    ) {
    const navData = this.activatedRoute.snapshot.queryParams;
    this.productObject = JSON.parse(navData.data);
    console.log('neteyey ', this.productObject.batch);

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.getAllSurveys();
    this.getAllSurveyRewards();
  }

  ngOnInit() {
    this.subProductForm = this.formBuilder.group({
      productId: [this.productObject.batch.productId, [Validators.required]],
      product_name: [this.productObject.batch.product_name, [Validators.required]],
      expiry_date: [this.productObject.batch.expiry_date, Validators.required],
      production_date: [this.productObject.batch.production_date, Validators.required],
      survey_id: [this.productObject.batch.surveyId, Validators.required],
      reward_id: [this.productObject.batch.rewardId, Validators.required],
    });
  }
  convertDate(date) {
    const Newdate = this._date.transform(date, 'dd/MM/yyyy');
    console.log('Newdate', Newdate);
    return Newdate;
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.subProductForm.invalid);
    if (this.subProductForm.invalid) {
      return;
    }
    this.loading = true;
    let reward_id = this.subProductForm.get('reward_id').value;
    this.subProductForm.get('reward_id').setValue(Number(reward_id));
    let survey_id = this.subProductForm.get('survey_id').value;
    this.subProductForm.get('survey_id').setValue(Number(survey_id));
    console.log(this.subProductForm.value);
    this.productService.update_subProduct(this.productObject.batch.batch_num, this.subProductForm.value)
      .pipe(first())
      .subscribe(
        dataRslt => {
          console.log('ok>>', dataRslt['body']);
          if (dataRslt['body'].status) {
            this.submitted = false;
            this.loading = false;
            let product = dataRslt['body'].data;
            this.router.navigate(['/products/product-batches'], { queryParams: product, skipLocationChange: true });
          } else {
            this.success_msg = dataRslt['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
  getAllSurveys() {
    this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log('result of survey', data['data']);
      if (data['data'].surveys) {
        console.log('okay ooo', data['data'].surveys);
        this.data = data['data'].surveys;
        this.save_survey = this.data;
        localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
      }
    });
  }
  getAllSurveyRewards() {
    this.surveyrewardServices.getAllSurveyReward(this.currentUser.id).pipe(first()).subscribe(data => {
      console.log('result of reward', data['data']);
      if (data['data'].rewards) {
        this.data = data['data'].rewards;
        this.save_surveyReward = this.data;
        console.log('rewards', this.save_surveyReward);
        localStorage.setItem('allsurveyRewards', JSON.stringify(data['data'].rewards));
      }
    });
  }
}
