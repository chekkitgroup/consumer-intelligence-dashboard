import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthenticationService, ProductService } from '../../_services';
import { first } from 'rxjs/operators';
import { User } from '../../_models';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ExportToCsv } from 'export-to-csv';
declare var jsPDF;
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-product-pins',
  templateUrl: './product-pins.component.html',
  styleUrls: ['./product-pins.component.css']
})
export class ProductPinsComponent implements OnInit {
  batchObject: any;
  productObject: any;
  navData: any;
  product_pin: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  loading = true;
  prevPin: any;
  nextPin: any;
  hasNext: boolean = false;
  hasPrevious: boolean = false;
  per_page: number = 500;
  pins: any;
  totalPins: number = 0;
  cursors: any;
  name = 'Angular 6';
  showCustomExport = false;

  @ViewChild('screen', {static:true}) screen: ElementRef;
  @ViewChild('canvas', {static:true}) canvas: ElementRef;
  @ViewChild('downloadLink', {static:true}) downloadLink: ElementRef;
  allPins: any;
  quantity: any;
  lastScanEndIndex = 0;
  success_msg: any;
  lastExportIndex: any;
  processing: boolean;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private productService: ProductService,
    private authenticationService: AuthenticationService, 
    private router: Router
  ) { 
    this.navData = this.activatedRoute.snapshot.queryParams;
    // console.log('product', JSON.parse(this.navData.data));
    console.log('product', (this.navData));

    this.productService.productDetails$.subscribe(
      (details) => {
        this.productObject = details;
        // console.log(details)
      }
    );

    this.productService.batchDetails$.subscribe(
      (details) => {
        this.batchObject = details;
        console.log(details)
      }
    );
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.getProductPins();
    this.getLastExportHistory();

  }

  
  async getProductPins() {
    await this.productService.getProductPins(this.batchObject.id, this.per_page,
      this.prevPin, this.nextPin).pipe(first()).subscribe(async data => {
        console.log('product-pin', data['data']);
        if (data['status']) {
          this.product_pin = await data['data'];
          this.pins = this.product_pin.pins;
          this.totalPins = this.product_pin.totalPins;
          this.loading = false;
          this.cursors = this.product_pin.cursors;
          this.nextPin = this.cursors.after;
          this.prevPin = this.cursors.before;
          this.hasNext = this.cursors.hasNext;
          this.hasPrevious = this.cursors.hasPrevious;
          window.scrollTo(0, 0)
          // await this.showToast(this.status, this.title, this.content);
        } else {
          this.loading = false;
          // await this.showToast(this.status, 'OH, sorry', 'Pls, try to refresh!');
        }
      });
  }
  async getLastExportHistory() {
    await this.productService.getLastExportHistory(this.batchObject.id).pipe(first()).subscribe(async data => {
        console.log('product-pin', data['data']);
        if (data['status']) {
          this.lastExportIndex =  data['data'].endIndex?data['data'].endIndex:0;
        } else {
          this.loading = false;
        }
      });
  }
  async getAllProductPins() {
    await this.productService.getAllProductPins(this.batchObject.id).pipe(first()).subscribe(async data => {
        console.log('product-pin', data['data']);
        if (data['status']) {
          this.product_pin = await data['data'];
          this.allPins = this.product_pin.pins;
          // await this.showToast(this.status, this.title, this.content);
        } else {
          this.loading = false;
          // await this.showToast(this.status, 'OH, sorry', 'Pls, try to refresh!');
        }
      });
  }
  getNextPin(): void {
    this.prevPin = '';
    console.log(this.nextPin)
    this.getProductPins();
  }
  getPrevPin(): void {
    this.nextPin = '';
    this.getProductPins();
  }
  ngOnInit() {

  }
  generatePin() {
    const data = JSON.stringify({
      batch: this.batchObject,
      product: this.productObject,
    });
    this.router.navigate(['/pages/new-pin'], { queryParams: { data }, skipLocationChange: true });
  }
  generateExcel(action = 'open') {
    var mappedPins = this.pins.map(p => ({ serial: '1000' + p.id, pin: p.pin_value }));

    console.log(mappedPins);


    var d = new Date();
    var n = d.getTime();
    let newFileName = 'chekkit-pin-sheet-' + n;

    const options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Chekkit Generated pin CSV',
      useTextFile: false,
      useBom: true,
      filename: newFileName,
      useKeysAsHeaders: false,
      headers: ['Serial Number', 'Pin']
    };

   

      const csvExporter = new ExportToCsv(options);
      
      csvExporter.generateCsv(mappedPins);    
  }
  generateAllExcel(action = 'open') {
    var mappedPins = this.allPins.map(p => ({ serial: '1000' + p.id, pin: p.pin_value }));

    console.log(mappedPins);


    var d = new Date();
    var n = d.getTime();
    let newFileName = 'chekkit-pin-sheet-' + n;

    const options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Chekkit Generated pin CSV',
      useTextFile: false,
      useBom: true,
      filename: newFileName,
      useKeysAsHeaders: false,
      headers: ['Serial Number', 'Pin']
    };

   

      const csvExporter = new ExportToCsv(options);
      
      csvExporter.generateCsv(mappedPins);    
  }

 
  generatePng(){
    html2canvas(this.screen.nativeElement).then(canvas => {
      this.canvas.nativeElement.src = canvas.toDataURL();
      this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      this.downloadLink.nativeElement.download = 'marble-diagram.png';
      this.downloadLink.nativeElement.click();
    });
      // only jpeg is supported by jsPDF
      // var canvas = document.getElementById('myCanvas');

      // var imgData = canvas.toDataURL("image/jpeg", 1.0);
      // var pdf = new jsPDF();

      // pdf.addImage(imgData, 'JPEG', 0, 0);
      // pdf.save("download.pdf");
  }
  
  generatePdf(){
    html2canvas(this.screen.nativeElement).then(canvas => {
      this.canvas.nativeElement.src = canvas.toDataURL();
      var imgData = canvas.toDataURL('image/jpeg', 1.0);
      // this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      this.downloadLink.nativeElement.download = 'marble-diagram.png';
      var pdf = new jsPDF();

      pdf.addImage(imgData, 'JPEG', 0, 0);
      pdf.save("download.pdf");

      // this.downloadLink.nativeElement.click();
    });
      // only jpeg is supported by jsPDF
      // var canvas = document.getElementById('myCanvas');

      // var imgData = canvas.toDataURL("image/jpeg", 1.0);
      // var pdf = new jsPDF();

      // pdf.addImage(imgData, 'JPEG', 0, 0);
      // pdf.save("download.pdf");
  }
  async getPinAndExport(q) {
    let prevPin = parseInt(this.lastExportIndex);
    let nextPin = parseInt(this.lastExportIndex) + parseInt(this.quantity);
    console.log(prevPin)
    this.processing = true;
    await this.productService.getProductPinsExport(this.batchObject.id, q,
      JSON.stringify(prevPin), JSON.stringify(nextPin)).pipe(first()).subscribe(async data => {
        console.log('product-pin', data['data']);
        if (data['status']) {
          var mappedPins = data['data'].pins.map(p => ({ serial: '10000000' + p.id, pin: p.pin_value }));
      
          console.log(mappedPins);
      
      
          var d = new Date();
          var n = d.getTime();
          let newFileName = 'chekkit-pin-sheet_' + prevPin + '-' + nextPin + '_'+ n;
      
          const options = { 
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalSeparator: '.',
            showLabels: true, 
            showTitle: true,
            title: 'Chekkit Generated pin CSV',
            useTextFile: false,
            useBom: true,
            filename: newFileName,
            useKeysAsHeaders: false,
            headers: ['Serial Number', 'Pin']
          };
      
         
      
            const csvExporter = new ExportToCsv(options);
            
            csvExporter.generateCsv(mappedPins);  
          
          
          this.processing = false;
          
          this.updatePinExportHistory();
          // await this.showToast(this.status, this.title, this.content);
        } else {
          this.processing = false;
          // await this.showToast(this.status, 'OH, sorry', 'could not get pins!');
        }
      });

  }

  updatePinExportHistory() {
    // let prevPin = parseInt(this.lastExportIndex) + parseInt(this.quantity);
    // let nextPin = parseInt(this.lastExportIndex) + parseInt(this.quantity);
    console.log(parseInt(this.lastExportIndex) + parseInt(this.quantity))
    let d = {
      sub_product_id: this.batchObject.id,
      quantity: this.quantity,
      start_index: parseInt(this.lastExportIndex),
      end_index: parseInt(this.lastExportIndex) + parseInt(this.quantity)
    };

    // this.loading = true;
    // console.log(this.surveyForm.value);
    this.productService.updatePinExportHistory( this.currentUser.id, d )
      .pipe(first())
      .subscribe(
        data => {
          // console.log("ok>>", data['data']);
          if (data['status']) {
            // this.success_msg = "Survey added successfully.";
            // this.router.navigate(['/pages/survey']);
            this.getLastExportHistory();
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          // this.alertService.error(error);
          // this.loading = false;
        });
  }
  

  async convert(value) {
    const dg = JSON.parse(value);
    const digits = '000000000000';
    const newDgt = digits.slice(0, -dg.length);
    console.log(digits, newDgt, 'newDgt + dg', newDgt + dg, dg.length);
    return await newDgt + dg;
  }
}
