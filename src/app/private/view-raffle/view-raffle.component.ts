import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Survey } from '../../_models/survey';
import { SurveyService } from '../../_services/survey.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-view-raffle',
  templateUrl: './view-raffle.component.html',
  styleUrls: ['./view-raffle.component.css']
})
export class ViewRaffleComponent implements OnInit {

  raffle: any;
  gifts: any;
  raffleStats: any;
  selectingWinner: boolean;
  loadingSelect: boolean;
  raffleWinner: any;
  raffleId: any;
  selectedGift: any;
  adding: boolean;
  

  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute, 
    private surveyService: SurveyService
    ) {
  }


  ngOnInit() {
    console.log(this.activatedRoute.snapshot.queryParams);
    let params = this.activatedRoute.snapshot.queryParams;
    let id = params.data;
    this.raffleId = id;
    console.log(params.data)
    console.log('get here');
    this.getRaffleDetails(id);
  }

  getRaffleDetails(id) {
    this.surveyService.getRaffleDetails(id).pipe(first()).subscribe(data => {
      console.log(data['data']);
      if (data['status']) {
        this.raffle = data['data'].raffle;
        this.gifts = data['data'].gifts;
        this.raffleStats = data['data'].raffleStats;
        console.log(this.raffle)
      }
    });
  }
  selectRaffleWinner(gift) {
    this.selectingWinner = true;
    this.loadingSelect = true;
    this.selectedGift = gift;
    
    this.surveyService.selectRaffleWinner(gift.id).pipe(first()).subscribe(data => {
      console.log(data['data']);
      if (data['status']) {

        this.loadingSelect = false;
        this.raffleWinner = data['data'].rWinner;
        this.getRaffleDetails(this.raffleId);
        console.log(this.raffleWinner)
      }
    });
  }

  closeSelectWinner() {
    this.selectingWinner = false;
    this.loadingSelect = false;
    this.selectedGift = undefined;
    this.raffleWinner = undefined;
  }

  toggleSelecting() {
    this.selectingWinner = !this.selectingWinner;
  }
}
