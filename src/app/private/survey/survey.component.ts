import { Component, ViewChild, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService, SurveyService, SurveyRewardService, AlertService, ProductService } from '../../_services';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { User } from '../../_models';
import { UserService } from '../../_services/user.service';
import { Survey } from '../../_models/survey';
import { Survey_Question } from '../../_models/survey_quest';
import { UtilityProvider } from '../../_helpers/utility';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {
  editing = {};
  rows = [];
  temp = [];
  success_msg: any;
  loadingIndicator = true;
  reorderable = true;
  closeResult: any;
  data: any;
  surveyForm: FormGroup;
  loading = false;
  submitted = false;
  save_survey: any;
  save_products: any;
  selectChange: any;
  currentUser: User;
  currentUserSubscription: Subscription;
  user_recordCounts: any;

  index = 1;
  subtitle: string;
  destroyByClick = true;
  duration = 5000;
  hasIcon = true;
  preventDuplicates = false;

  title = 'HI there!';
  content = `payment successful!`;

  constructor(
    public utility: UtilityProvider, 
    private router: Router,
    private authenticationService: AuthenticationService, 
    private surveyService: SurveyService,
    private userService: UserService
    ) {

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.user;
      console.log('User value', this.currentUser);
    });
    this.getAllSurvey();
    setTimeout(() => { this.loadingIndicator = false; }, 1500);
  }
  getAllSurvey() {
    this.authenticationService.allsurveys.subscribe(save_survey => {
      console.log(save_survey);
      console.log('printed Surveys up');
      if (save_survey) {
        this.save_survey = save_survey;
        this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
          if (data['data'].surveys) {
            this.save_survey = data['data'].surveys;
            // localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
          }
        });
      } else {
        this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
          console.log(data['data'].surveys);
          if (data['data'].surveys) {
            this.save_survey = data['data'].surveys;
            localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
          }
        });
      }
    });
  }
  ngOnInit() {
    this.getUserRecordCount();
  }
  getUserRecordCount() {
    this.authenticationService.recordCounts.subscribe(all_counts => {
      if (all_counts) {
        console.log('all counts', all_counts);
        this.user_recordCounts = all_counts;
        this.userService.getUserRecordCount(this.currentUser.id).pipe(first()).subscribe(async all_counts => {
          console.log('all counts>>>>', all_counts);
          if (all_counts['status']) {
            this.user_recordCounts = await all_counts['data'];
            localStorage.setItem('recordCounts', JSON.stringify(this.user_recordCounts));
          }
        });
      } else {
        this.userService.getUserRecordCount(this.currentUser.id).pipe(first()).subscribe(async all_counts => {
          console.log('all counts', all_counts);
          if (all_counts['status']) {
            this.user_recordCounts = await all_counts['data'];
            localStorage.setItem('recordCounts', JSON.stringify(this.user_recordCounts));
          }
        });
      }
    });
  }
  getSurveyQuestions(survey_data: Survey) {
    this.surveyService.getSurveyQuestions(survey_data.id).pipe(first()).subscribe(data => {
      console.log(data['data']);
      if (data['status']) {
        return data['data'];
      }
    });
  }
  scheduleSurvey(){
    this.utility.showToast('info','Please note that this feature will be available soon')
  }
  gotoViewSurvey(surveyData: Survey) {
    this.router.navigate(['/dashboard/surveys/view-survey'], { queryParams: surveyData });

  }
  gotoEditSurvey(surveyData: Survey, questions: Survey_Question) {

    const data = JSON.stringify({
      survey: surveyData,
      question: questions,
    });

    this.router.navigate(['/dashboard/surveys/edit-survey'], { queryParams: { data }, skipLocationChange: false });

  }
  gotoDeleteSurvey(surveyData: Survey) {
    this.router.navigate(['/dashboard/surveys/edit-survey'], { queryParams: surveyData, skipLocationChange: true });

  }
}
