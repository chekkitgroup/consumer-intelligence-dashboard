import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../_services';

import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
//declare var $: any;

@Component({
  selector: 'app-full-layout',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit {

	public config: PerfectScrollbarConfigInterface = {};
  closed = false;
  currentUserPlan: any;
  currentUserSubscription: any;

  	constructor(
      public router: Router,
      private authenticationService: AuthenticationService,
      ) {


        this.currentUserPlan = this.authenticationService.currentUserPlan.subscribe(data => {
          // this.currentUser = user.user;
          console.log('User plan', data);
        });

        this.authenticationService.currentSubscription.subscribe(data => {
          this.currentUserSubscription = data;
          // console.log(this.diffDays(data.end_date))
          console.log('User subscription', data);
        });
    }

    diffDays(d1)
    {
      d1 =   new Date(d1);  // Today
      // console.log(d1);

      var d2 =   new Date();              // Today

      var ndays;
      var tv1 = d1.valueOf();  // msec since 1970
      var tv2 = d2.valueOf();
    
      ndays = (tv1 - tv2) / 1000 / 86400;
      ndays = Math.round(ndays - 0.5);
      return ndays;
    }

	public innerWidth: number=0;
	public defaultSidebar: string='';
	public showMobileMenu = false;
	public expandLogo = false;
	public sidebartype = 'full';

  Logo() {
    this.expandLogo = !this.expandLogo;
  }

  ngOnInit() {
    if (this.router.url === '/') {
      this.router.navigate(['/starter']);
    }
    this.defaultSidebar = this.sidebartype;
    this.handleSidebar();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.handleSidebar();
  }

  handleSidebar() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 1170) {
      this.sidebartype = 'mini-sidebar';
    } else {
      this.sidebartype = this.defaultSidebar;
    }
  }

  toggleSidebarType() {
    switch (this.sidebartype) {
      case 'full':
        this.sidebartype = 'mini-sidebar';
        break;

      case 'mini-sidebar':
        this.sidebartype = 'full';
        break;

      default:
    }
  }
}
