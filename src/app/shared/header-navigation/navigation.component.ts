import { Component, EventEmitter, Output } from '@angular/core';
//declare var $: any;
import { User } from '../../_models/user';
import { map, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent {
  @Output()
  toggleSidebar = new EventEmitter<void>();

  public showSearch = false;

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  userMenu = [];
  name: string;
  subtitle: string;
  currentUser: User;
  currentUserSubscription: Subscription;
  users: User[] = [];


  constructor( 
    private router: Router,
    private authenticationService: AuthenticationService) {
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        console.log(user);
        console.log(this.currentUser);

        if(user){
          this.currentUser = user.user;
        }
        if (!this.currentUser.first_name) {
          this.currentUser.first_name = 'User';
        }
      });

    }
    logout() {
      this.authenticationService.logout();
      this.router.navigate(['/authentication/login']);
    }
    profile() {
      this.router.navigate(['dashboard/profile']);
    }
}
