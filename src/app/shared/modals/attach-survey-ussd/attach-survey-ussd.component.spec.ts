import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachSurveyUssdComponent } from './attach-survey-ussd.component';

describe('AttachSurveyUssdComponent', () => {
  let component: AttachSurveyUssdComponent;
  let fixture: ComponentFixture<AttachSurveyUssdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachSurveyUssdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachSurveyUssdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
