import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../_services/rest.service';
import { first } from 'rxjs/operators';
import 'rxjs/Rx';
import { AlertService,AuthenticationService, SurveyService } from '../../../_services';
import { ToastService } from '../../../_services/toast.service';
import {UtilityProvider} from "../../../_helpers/utility";
import { Survey } from '../../../_models/survey';
import { UserService } from '../../../_services/user.service';
import { NgbModal, ModalDismissReasons, NgbActiveModal,NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'ngx-attach-survey-ussd',
  templateUrl: './attach-survey-ussd.component.html',
  styleUrls: ['./attach-survey-ussd.component.scss']
})
export class AttachSurveyUssdComponent implements OnInit {
	inputData = {
		short_code: "",
		survey_id: "",
	};
  loading: boolean;
  currentUser: any;
  success = false;
  surveys: any;
  currentUserSubscription: any;
  submitted: boolean;
  success_msg: any;
  submitting = false;
  
  // End the Closeable Alert
  // This is for the self closing alert
  private _message = new Subject<string>();
  staticAlertClosed = false;
  responseMessage: string;
  messageType:any;
  processing: boolean;
  loadingCode: boolean;


  constructor(
    public utility: UtilityProvider,
    private authenticationService: AuthenticationService, 
    private surveyService: SurveyService,
    private modalService: NgbModal,
    private toastService: ToastService, 
    private _rest: RestService, 
  ) { 
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user.data.user;
      console.log('User value', this.currentUser);
    });
    // set up Alert
      setTimeout(() => (this.staticAlertClosed = true), 20000);

      this._message.subscribe(message => (this.responseMessage = message));
      this._message.pipe(debounceTime(5000)).subscribe(() => (this.responseMessage = null));
  }

  ngOnInit() {
    this.get3DigitCode()
    this.getAllSurvey()
  }

  getAllSurvey() {
    this.loading = true;
    this.authenticationService.allsurveys.subscribe(save_survey => {
      console.log(save_survey);
      console.log('printed Surveys up');
      if (save_survey) {
        this.surveys = save_survey;
        this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
          if (data['data'].surveys) {
            this.surveys = data['data'].surveys;
            this.loading = false;
          }
        });
      } else {
        this.surveyService.getAllUserSurveys(this.currentUser.id).pipe(first()).subscribe(data => {
          console.log(data['data'].surveys);
          if (data['data'].surveys) {
            this.surveys = data['data'].surveys;
            localStorage.setItem('allsurveys', JSON.stringify(data['data'].surveys));
            this.loading = false;
          }
        });
      }
    });
  }
  get3DigitCode() {
    this.loadingCode = true;
    this._rest.get3DigitCode().pipe(first()).subscribe(data => {
      if (data['status']) {
        // this.plan = data['data'].plan;
        this.inputData.short_code = data['data'].short_code;
        this.loadingCode = false;
        console.log(this.inputData.short_code)
      }
    },
    error => {
      console.log(error)
      this.loadingCode = false;
    });
  }

  onSubmit() {
    this.submitted = true;
    this.processing = true;
    console.log(this.inputData);
    this._rest.requestUssdSurveyAttachment(this.inputData)
      .pipe(first())
      .subscribe(
        data => {
          console.log("ok>>", data);
          if (data['status']) {
            this.resetForm()
            this.processing = false;
            this.messageType = 'success';
            this._message.next(`Your request has been submitted`);


            let env = this;
            setTimeout(function(){
              env.modalService.dismissAll(JSON.stringify(env.inputData.short_code))
            }, 2000)
          }
          else {
            this.success_msg = data['message'];
            this.processing = false;
          }
        },
        error => {
          console.log(error)
          this.messageType = 'danger';
          this._message.next(`${error}`);
          // this.alertService.error(error);
          this.processing = false;
        });
  }
  close(){
    this.modalService.dismissAll()
  }

  resetForm(){
    this.inputData = {
      short_code: "",
      survey_id: "",
    };
  }
}
