import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestService } from '../../../_services';
import { AlertService } from '../../../_services'
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'ngx-add-reward',
  templateUrl: './add-reward.component.html',
  styleUrls: ['./add-reward.component.scss']
})


export class AddRewardComponent implements OnInit {
  rewardForm: FormGroup;
  addData: any;
  loading = false;
  submitted = false;
  success_msg: any;
  processing = false;
  

  // End the Closeable Alert
  // This is for the self closing alert
  private _message = new Subject<string>();
  staticAlertClosed = false;
  responseMessage: string;
  messageType:any;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private modal: NgbModal,
    private _rest: RestService ) {
        // set up Alert
      setTimeout(() => (this.staticAlertClosed = true), 20000);

      this._message.subscribe(message => (this.responseMessage = message));
      this._message.pipe(debounceTime(5000)).subscribe(() => (this.responseMessage = null));
    }


  ngOnInit() {
    this.rewardForm = this.formBuilder.group({
      reward_value: ['', Validators.required],
      reward_type: ['', Validators.required],
      reward_point: ['', Validators.required],
      reward_quant: ['', Validators.required],
      image: ['', Validators.required]
    });
  }


  get f() { return this.rewardForm.controls; }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.rewardForm.get('image').setValue(file);
    }
  }

  resetForm() {
    this.rewardForm.setValue({
      reward_value: '',
      reward_type: '',
      reward_point: '',
      reward_quant: '',
      image: ''
    });
  }


  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.rewardForm.invalid);
    // if (this.rewardForm.invalid) {
    //   return;
    // }
    this.processing = true;
    console.log(this.rewardForm.value);
    const formData = new FormData();
    formData.append('reward_value', this.rewardForm.get('reward_value').value);
    formData.append('reward_type', this.rewardForm.get('reward_type').value);
    formData.append('reward_point', this.rewardForm.get('reward_point').value);
    formData.append('reward_quant', this.rewardForm.get('reward_quant').value);
    formData.append('photo', this.rewardForm.get('image').value);

    this._rest.addRewards(formData)
      .pipe(first())
      .subscribe(
        data => {
          console.log('ok>>', data);
          if (data['status']) {
            this.resetForm();
            this.processing = false;
            this.messageType = 'success';
            this._message.next(`Your request has been submitted`);
            console.log('oka 3 >>');
            let env = this;
            setTimeout(function(){
              env.closeModal()
            }, 2000)
            this.loading = false;
          }
          else {
            this.success_msg = data['message'];
          }
        },
        error => {
          this.alertService.error(error);
          this.processing = false;
        });
  }
 
  closeModal(){
    this.modal.dismissAll()
  }
}
 