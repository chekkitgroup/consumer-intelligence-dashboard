import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttachSurveyUssdComponent } from './modals/attach-survey-ussd/attach-survey-ussd.component';
import { AddRewardComponent } from './modals/add-reward/add-reward.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AttachSurveyUssdComponent,
    AddRewardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
