import { Component, AfterViewInit, OnInit } from '@angular/core';
import { ROUTES } from './menu-items';
import { RouteInfo } from './sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//declare var $: any;
import { AuthenticationService } from '../../_services';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  showMenu = '';
  showSubMenu = '';
  public sidebarnavItems:RouteInfo[]=[];
  // this is for the open close
  addExpandClass(element: string) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute
  ) {}

  // End open close
  ngOnInit() {
    const homeLink:RouteInfo = {
                      path: '/dashboard',
                      title: 'Overview',
                      icon: 'mdi mdi-home-outline',
                      class: '',
                      code:'3',
                      extralink: false,
                      submenu: []
                    };

    let allNavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);

      
    this.authenticationService.currentUserPlan.subscribe(data => {
            console.log(data.features)

        let plan = JSON.parse(data.features);
        // console.log(plan)

        this.sidebarnavItems = allNavItems.filter(({
          code
        }) => plan.includes(code));
        this.sidebarnavItems.unshift(homeLink);
      });

      // console.log(allNavItems)
      console.log(this.sidebarnavItems)

  }
}
