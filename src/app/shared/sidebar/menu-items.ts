import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = 
[
  {
    path: '/dashboard',
    title: 'Overview',
    icon: 'mdi mdi-home-outline',
    class: '',
    code:'1',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/analytics',
    title: 'Analytics',
    icon: 'mdi mdi-chart-bar',
    class: '',
    code:'2',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/products',
    title: 'Products',
    icon: 'mdi mdi-wallet-giftcard',
    class: '',
    code:'3',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/insights',
    title: 'Insights',
    icon: 'mdi mdi-chart-bubble',
    class: '',
    code:'4',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/surveys',
    title: 'Surveys',
    icon: 'mdi mdi-checkbox-marked-outline',
    class: '',
    code:'5',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/rewards',
    title: 'Rewards',
    icon: 'mdi mdi-gift',
    class: '',
    code:'6',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/code-collections',
    title: 'Code Collections',
    icon: 'mdi mdi-qrcode',
    class: '',
    code:'7',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/user-feedback',
    title: 'User Feedback Logs',
    icon: 'mdi mdi-message-text',
    class: '',
    code:'8',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/retargeting',
    title: 'Engage Customers',
    icon: 'mdi mdi-table',
    class: '',
    code:'9',
    extralink: false,
    submenu: []
  }
];
