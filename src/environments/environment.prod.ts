export const environment = {
  production: true,
  apiUrl: 'https://ci-api.chekkit.app',
  // apiUrl: 'https://ci-api-staging.chekkit.app',
  // apiUrl: 'http://54.191.192.249:3000',

  PORT: 3000,
  basePath: '/api/v1'
};
